# post-facto-calibration

This repo can be found and "git clone"d at:

https://codeberg.org/markgalassi/post-facto-calibration

## View our Findings

Take a look at the "results" directory to view our presentation and research paper on the results of our research. 
I'd (Michael Bengil/Archan6el) suggest you primarily look at the research paper


## To run some of our simulations

You can look at codedaperture/README.md


## What is documented?

A study of the problem of post-facto calibration of instruments and
some techniques to solve it.

For a very quick glance at the "statement of the problem" please see
the file ``doc/source/statement-of-problem.rst``

Full docs are in doc subdirectory.  I (markgalassi) keep a built copy
that you can view at:

https://markgalassi.codeberg.page/post-facto-calibration/

But the best is for you to build your own up-to-date copy, which you
can do with:
```
pip3 install sphinx
pip install sphinx-rtd-theme
git clone [... the link that codeberg gives you to clone ...]
cd post-facto-calibration/doc
make html
```
and then point your browser to build/html/index.html

You can also build docs for printing with:
```
make latexpdf
```
and then point your PDF viewer to
``build/latex/post-factocalibration.pdf``

You can also view a current copy at
https://markgalassi.codeberg.page/post-facto-calibration/ -- TIP: this
is built with
```
(cd ~/repo/post-facto-calibration/doc && make html && rsync -avz --delete build/html/ ~/repo/markgalassi.codeberg.page/post-facto-calibration)
(cd ~/repo/markgalassi.codeberg.page/ && git add post-facto-calibration && git add -u post-facto-calibration && git commit -a -m "updating web deployment" && git push)
```
