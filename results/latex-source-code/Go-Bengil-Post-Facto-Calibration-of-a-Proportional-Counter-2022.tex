\documentclass[twoside,twocolumn]{article}
\documentclass[oneside]{picture}
\usepackage{hanging}
\usepackage[sc]{mathpazo}
\usepackage[T1]{fontenc} 
\linespread{1.05} 
\usepackage{microtype} 
\usepackage[english]{babel}
\usepackage[hmarginratio=1:1,top=28mm,columnsep=20pt]{geometry} 
\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} 
\usepackage{booktabs} 
\usepackage{lettrine}
\usepackage{graphicx} 
\usepackage{enumitem} 
\setlist[itemize]{noitemsep} 
\usepackage{abstract} 
\renewcommand{\abstractnamefont}{\normalfont\bfseries}
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} 
\usepackage{titlesec}
\renewcommand\thesection{\Roman{section}} 
\renewcommand\thesubsection{\roman{subsection}} 
\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{}
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} 
\usepackage{fancyhdr} 
\pagestyle{fancy}
\fancyhead{} 
\fancyfoot{} 
\fancyhead[C]{Post-Facto Calibration of a Proportional Counter $\bullet$ \today }
\fancyfoot[RO,LE]{\thepage} 
\usepackage{titling} 
\usepackage{hyperref} 
\title{ \Huge Post-Facto Calibration of a Proportional Counter}
\author{Michael Ace Bengil and Ye-un Go}
\date{August 4, 2022} 
\renewcommand{\maketitlehookd}{
\begin{abstract}
\noindent A 1-Dimensional position-sensitive proportional counter is a scientific instrument that can detect ionizing particle radiation, such as photons from the X-ray spectrum. Coded apertures are physical mask grids with either intentional or random patterns made of materials that are non-penetrable to a target wavelength of electromagnetic radiation, such as X-rays. The inevitable decline of the proportional counter’s performance over time becomes an issue with limited opportunities for physical re-calibration, especially in a space-based environment. This problem creates a need for an internal software calibration, or ‘post-facto’ calibration. In our research, we simulate a variety of controlled and randomized distortion on the readout of X-rays running through a unique coded aperture and being detected by the proportional counter. The method of optimization used to calibrate and fix the distorted outputs is through a genetic algorithm.
\end{abstract}
}
\begin{document}
\maketitle
\section{Introduction}
\lettrine[nindent=0em,lines=3]{T} 
\normalsize his research consists of an instrument used to detect incoming X-rays, which is made up of two main components: a 1-Dimensional position-sensitive proportional counter and a coded aperture. The coded aperture placed over the proportional counter allows the incoming X-rays to form a unique pattern reflective of the coded aperture’s pattern [1], and the proportional counter calculates the positions hit by the X-rays on its detector. The readouts of the proportional counter are the 1-Dimensional positions in which the X-ray photons have been detected. Real position values are where the X-ray photons physically hit the proportional counter’s detector, and distorted position values are what the proportional counter measures the positions of the incoming X-rays to be. Distorted position values depend on the proportional counter’s calibration and the physical condition of the proportional counter. In order to have consistently-matching real position values and distorted position values, our research consists of methods for post-facto calibration on proportional counters.
\section{Simulating the Experiment}
Virtually simulating the experiment encompasses the simulation of two main components (proportional counter and coded aperture), the volume and pattern of incoming X-rays, and artificial distortion of the X-ray readout of the proportional counter.\\
\subsection{Simulating the Environment}
All components and environments are virtually simulated. Dimensional measurements of the simulated components (coded aperture and proportional counter) reflect the actual dimensional measurements of the Wide-Field X-ray Monitor carried on the High Energy Transient Explorer 2 (HETE-2) satellite launched on 2000 October 9 [2]. Our paper extends the research on the performances of the Wide-Field X-ray Monitor in attempt to achieve post-facto calibration [3]. The pattern of the simulated coded aperture is 20.8 cm long and composed of seventy different binary values: zero represents a closed bin and one represents an opened bin. The proportional counter’s detector is 12.0 cm long and composed of 420 bins. To account for the difference in length and scale of the coded aperture and the proportional counter’s detector, a resampled 420-binned coded aperture is extrapolated from the original 70-binned coded aperture. \\
\subsection{Simulating the Imperfection}
Our simulation must mirror reality as closely as possible, and in reality, our proportional counter isn’t perfect. Two methods of simulating imperfection are present: a distortion function and noise fraction. The distortion function takes in the individual counts of X-ray photons in each bin of the detector and outputs a distorted version of the count. The distortion function is a modified form of a sine function, and the degree of distortion is proportional to a distortion dial variable. The noise fraction simulates the proportional counter picking up wrong electromagnetic waves. A higher noise fraction leads to increased detection of ionizing particles other than X-rays, blunting the accuracy of the readout. Some built-in parameters are represented as modifiable variables, such as the number of incoming X-ray photons and the angle at which the photons are hitting the coded aperture. \\
\section{In-Lab Calibration}
With the foundation of simulating the experiment all laid, we can now run a normal simulation. Because we have control over every aspect of the experiment, we are knowledgeable of the real position values as well as the distortion that produces the distorted position values. In an ideal setting, plotting the ‘real position’ versus ‘distorted measured position’ should output a linear line; in other words, the two values should be equal to each other. However, due to the distortions, a non-linear relationship is likely produced. The resulting ‘s-curve,’ or nonlinear sigmoid relation, is in need of curve fitting to calculate the regression line function and its parameters which will be used to calibrate the proportional counter’s readout.
\begin{figure}
    \centering
    \includegraphics[width=6cm]{ideal-linear.png}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=6cm]{reality-sigmoid.png}
\end{figure}
The regression function of the sigmoid dot plot was done with a logistic function with four parameters as shown below [4]. This logistic function is our calibration function, though take note that the calibration function can/will change depending on what the distortion function is. 
\begin{figure}
    \centering
    \includegraphics[width=6cm]{curve-fit.png}
\end{figure}
Through the curve fit, we get the parameters for the calibration function that can calibrate the distorted positions, fixing the distortion. This  process simulates the method of calibration that researchers would perform in lab before deploying the proportional counter. 
\section{Optimization}
The ultimate goal of post-facto calibration is to optimize the parameters used in our calibration function in order to have the distorted position values from the proportional counter readout match the real position values. Despite imposing an artificial distortion to the readout, the optimization algorithm is developed completely blinded to the type/degree of distortion and the correct real position values.
All files for the entire project are found in our Codeberg repository [10]. All programs on the repository are written in Python on a Linux operating system. Python libraries used for our code are NumPy, MatPlotLib, SciPy, PyGAD, ArgParse ([5], [6], [7], [8] and [9]).
\subsection{Identifying the Metric}
Before developing the optimization algorithm, we first need a standard ‘metric’ to optimize for. The metric determines if the output of the optimization algorithm, or the ‘corrected’ distorted position values, is indeed correct or satisfactory. The metric will later be used in the fitness function of the genetic algorithm. Our metric is calculated by getting the difference of heights between the tallest peak and the second tallest peak of the cross-correlation graph between the readout of the proportional counter and the coded aperture. The cross-correlation graph has a clear tallest peak when there is no distortion on the readout. The cross-correlation graph has multiple competing candidates for the tallest peak with more distortion on the readout. The metric was determined as such to utilize the peak pattern properties of the cross-correlation graph. \\
\begin{figure}
    \centering
    \includegraphics[width=6cm]{peak.png}
    \caption{Top graph illustrates no distortion. Bottom graph illustrates maximum distortion.}
\end{figure}
\subsection{Optimizing with Genetic Algorithm}
The genetic algorithm [8] is a stochastic optimization algorithm inspired by Charles Darwin’s theory of natural evolution. A parent population, or a group of potential candidates that may resemble the real position values to varying degrees, is evaluated for its validity with the metric. Individuals with the highest metric will produce offspring that resemble certain successful aspects of themselves, just like Darwin’s theory on the survival and reproduction of the fittest. The resulting children population then takes on the roles of their parents and continuously produces more successful generations until an individual with the highest possible metric is found. There is a trade-off between the quality of offspring and run time: the more counts of produced generations there are, the more precise and ‘fit’ the offspring become at the expense of time. \\
\subsection{Putting it Together}
Through the genetic algorithm our metric is optimized. Populations are generated with solutions consisting of parameters that can be used with our calibration function to transform our distorted positions into estimates of the actual positions. These estimated, or “calibrated” positions can be turned into a readout and cross correlation with our mask. Our metric is calculated from this cross correlation. This process is repeated until we have a list of calibrated positions and an associated calibrated readout with the highest possible metric \\


\section{Findings}
The genetic algorithm and our defined metric succeeds in generating calibrated positions and subsequent calibrated readouts that result in the highest possible metric when the calibrated readout is cross correlated with our mask. (Figure 5, next page)\\


However, something quite intriguing was discovered. In numerous cases, the cross correlation between the mask and calibrated readout would be better than the cross correlation between the mask and actual readout. This implies that our calibration method does not only correct distortion, but other kinds of imperfections as well, to the point that our calibrated results perform better than the original. (Figure 2) \\


To test this theory, noise was introduced due to it being another form of imperfection. Amazingly, our calibration method was indeed still able to generate a calibrated readout that once cross correlated with the mask, resulted in a higher metric and better graph than both the distorted and actual readouts. (Figures 3 and 4)\\



Through this research, we developed a calibration method that not only corrects our distortion, but also other imperfections as well. In the future, further scrutiny into what those other imperfections are and why our calibration method is able to correct them would be very valuable to look into \\


\begin{figure}
    \centering
    \includegraphics[width=7cm]{findings-two.png}
    \caption{Intriguing Results}
\end{figure} 

\begin{figure}
    \centering
    \includegraphics[width=7cm]{findings-three.png}
    \caption{Calibration given imperfection 1}
\end{figure} 

\begin{figure}
    \centering
    \includegraphics[width=7cm]{findings-last.png}
    \caption{Calibration given imperfection 2}
\end{figure} 



\pagebreak

\begin{figure}
    \includegraphics[width=15cm]{findings-large.png}
    \caption{Expected Results}
\end{figure} 





\clearpage
\section{References}
\begin{hangparas}{.25in}{1}
    [1] Zand, J, Coded aperture camera imaging concept, National Aeronautics and Space Administration Goddar Space Flight Center, 7 March 1996, https://asd.gsfc.nasa.gov/archive/cai/coded_intr.html
    
    [2] Shirasaki, Y. et al., Design and Performance of the Wide-Field X-Ray Monitor on Board the High-Energy Transient Explorer 2, Publications of the Astronomical Society of Japan, Volume 55, Issue 5,25 October 2003, Pages 1033–1049, https://doi.org/10.1093/pasj/55.5.1033
    
    [3] Kawai, N. et al., Wide-field X-ray Monitor on HETE-2, Astronomy & Astrophysics Supplement Series,September 1999, Pages 563-564, https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.474.9643&rep=rep1&type=pdf
    
    [4] Wood, T, Sigmoid Function, DeepAI, deepai.org/machine-learning-glossary-and-terms/sigmoid-function#:~:text=Logistic%20sigmoid%20function%20in%20logistic%20regression&text=It%20outputs%20a%20probability%20value,is%20either%200%20or%201.
    [5] NumPy, numpy.org/.
    
    [6] MatPlotLib, matplotlib.org/.
    
    [7] SciPy, scipy.org/.
    
    [8] PyGad, pygad.readthedocs.io/en/latest/
    
    [9] ArgParse, docs.python.org/3/library/argparse.html
    
    [10] codeberg.org/markgalassi/post-facto-calibration
\end{document}
