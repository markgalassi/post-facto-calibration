====================
 Background reading
====================

Some hasty notes on resources:

General
=======

All the papers in the ``papers`` directory at the top level of this
repository.  These have not yet been sorted or carefully curated.

https://en.wikipedia.org/wiki/Proportional_counter

S-curve in agriculture:
https://en.wikipedia.org/wiki/Van_Genuchten%E2%80%93Gupta_model
has interesting functional forms for s-curves, although they are
flipped, which can probalby be fixed.

Distorion in optics:

https://en.wikipedia.org/wiki/Distortion_(optics)

has a section on correction and it shows great pictures of barrel
distortion in an ENIAC computer:

https://en.wikipedia.org/wiki/Distortion_(optics)#Software_correction

Image quality
=============

https://en.wikipedia.org/wiki/Image_quality -- has a discussion of
"objective methods".  The NR (No-reference) methods seem interesting.

https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio -- PSNR is
a quality measure, *but* it seems to require the original image.

No-reference image and video quality assessment: a classification and
review of recent approaches --
https://jivp-eurasipjournals.springeropen.com/articles/10.1186/1687-5281-2014-40
-- comparison of no-reference quality mesaures.  Does not show how to
calculate them, but has lots of background material.

"Automatic Image Quality Assessment in Python" --
https://towardsdatascience.com/automatic-image-quality-assessment-in-python-391a6be52c11
-- discusses python code for both reference-based and no-reference.
They then use tensorflow to get a metric.  --
https://github.com/krshrimali/No-Reference-Image-Quality-Assessment-using-BRISQUE-Model

https://www.researchgate.net/publication/276271541_No-reference_image_quality_assessment_algorithms_A_survey

https://paperswithcode.com/paper/rankiqa-learning-from-rankings-for-no#code

https://www.quora.com/What-are-some-measures-of-image-quality -- some
answers on the subject

"technical report: measuring digital image quality" Lundstrom --
https://www.semanticscholar.org/paper/Technical-report%3A-Measuring-digital-image-quality-Lundstr%C3%B6m/a2d3862705b7b192d5044761a84fc128f141bf1e
-- discussion with good pictures of the various types of distortion

"Image Quality Assessment Based on Intrinsic Mode Function
Coefficients Modeling" --
https://www.researchgate.net/publication/221014346_Image_Quality_Assessment_Based_on_Intrinsic_Mode_Function_Coefficients_Modeling
-- nicely laid out description of the various distortions, with
examples.

"Sharpness: What is it and How it is Measured" --
https://www.imatest.com/docs/sharpness/ -- discussion of how you
measure sharpness.  nice pictures.

"Is this right way to get Modulation Transfer Function(MTF)?" --
https://www.researchgate.net/post/Is_this_right_way_to_get_Modulation_Transfer_FunctionMTF
-- the answers contain practical tips on calculating MTFs, like
pointers to https://github.com/u-onder/mtf.py

"Introduction to Modulation Transfer Function" --
https://www.edmundoptics.com/knowledge-center/application-notes/optics/introduction-to-modulation-transfer-function/
-- pedagogical on how to calculate MTF.  They also make this video:
https://www.youtube.com/watch?v=mjGczJTkWic

"Measures of image quality" --
https://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/VELDHUIZEN/node18.html
-- introduces PSNR which "PSNR is a good measure for comparing
restoration results for the same image, but between-image comparisons
of PSNR are meaningless."  This might work well for us.


Point Spread Function (PSF)
===========================

https://en.wikipedia.org/wiki/Blind_deconvolution -- this might be
very close to what we are discussing

https://en.wikipedia.org/wiki/Point_spread_function -- general
discussion of 2D image errors

https://en.wikipedia.org/wiki/Deconvolution#Optics_and_other_imaging
-- the actual technique used to reverse the PSF

https://www.researchgate.net/publication/51797897_Measuring_and_interpreting_point_spread_functions_to_determine_confocal_microscope_resolution_and_ensure_quality_control
-- a careful procedure for measuring PSFs

"A Workingperson’s Guide to Deconvolution in Light Microscopy" --
https://www.future-science.com/doi/10.2144/01315bi01 -- general
discussion of possible distortions.

"DeconvolutionLab2: An open-source software for deconvolution
microscopy" --
https://www.sciencedirect.com/science/article/pii/S1046202316305096 --
an extensive paper describing the devoncovlution techniques and their
s/w.

"Point Spread Function (PSF) Measurement for Cell Phone Camera with a
High Resolution PSF of the Imaging Lens and a Sub-pixel Digital
Algorithm" --
https://www.researchgate.net/publication/253510393_Point_Spread_Function_PSF_Measurement_for_Cell_Phone_Camera_with_a_High_Resolution_PSF_of_the_Imaging_Lens_and_a_Sub-pixel_Digital_Algorithm
-- discussion of how to measure it for cell phone cameras

"Accurate Subpixel Point Spread Function Estimation from scaled image
pairs" -- https://hal.archives-ouvertes.fr/hal-00624757v2/document --
more discussion of specific cameras

"Computational Imaging Approach to Recovery of Target Coordinates Using Orbital Sensor Data" --
https://trace.tennessee.edu/cgi/viewcontent.cgi?article=5885&context=utk_graddiss
-- PhD dissertation from 2017. discusses many types of distortion and
discusses PSFs at length.  Page 60 lists two approaches to calibration
to correct for distortion.  Page 64 has an intriguing discussion of
Demon's Method (named after Maxwell's demon).  It also has distortion
pictures.  Page 72 has discussions, graphs, and example pictures of
the various types of noise.  Then he goes on to discuss methods of
mitigation.  Then p. 80 introduces "super resolution" (multiple
low-res -> high-res).

"Image Restoration for Under-Display Camera" --
https://openaccess.thecvf.com/content/CVPR2021/papers/Zhou_Image_Restoration_for_Under-Display_Camera_CVPR_2021_paper.pdf
-- discussions of PSFs with some interesting pictures


Thinking about writing code
===========================

https://pillow.readthedocs.io/en/stable/reference/ImageFilter.html#module-PIL.ImageFilter

https://pillow.readthedocs.io/en/stable/reference/ImageEnhance.html#example-vary-the-sharpness-of-an-image

the next few talk about the gkern() function in python:

https://stackoverflow.com/questions/29731726/how-to-calculate-a-gaussian-kernel-matrix-efficiently-in-numpy/29731818#29731818

https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html?highlight=gaussian_filter#scipy.ndimage.gaussian_filter

https://stackoverflow.com/questions/29731726/how-to-calculate-a-gaussian-kernel-matrix-efficiently-in-numpy/29733495#29733495

https://stackoverflow.com/questions/29731726/how-to-calculate-a-gaussian-kernel-matrix-efficiently-in-numpy/29731818#29731818

then some PSF stuff from astropy and photoutils:

https://stackoverflow.com/questions/55883497/how-do-you-extract-a-point-spread-function-from-a-fits-image

https://www.astropy.org/

https://photutils.readthedocs.io/en/stable/

https://docs.astropy.org/en/stable/convolution/index.html

https://docs.astropy.org/en/stable/convolution/kernels.html

conceptual on how to build your own:

https://photutils.readthedocs.io/en/stable/epsf.html#build-epsf

this one:

https://photutils.readthedocs.io/en/stable/psf.html

shows how to simulate an ugly PSF

this one:

https://stackoverflow.com/questions/55883497/how-do-you-extract-a-point-spread-function-from-a-fits-image

shows how to subtract a PSF fit and get a residual:
https://i.stack.imgur.com/6kgd7.png

https://www.researchgate.net/post/How-to-code-Point-Spread-Function-and-Modulation-Transfer-Function-via-python

https://github.com/habi/GlobalDiagnostiX/blob/master/MTF.py

https://www.researchgate.net/post/Can-anyone-recommend-software-to-measure-the-MTF-of-a-digital-image


