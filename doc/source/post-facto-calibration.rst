=======================
Post-Facto Calibration
=======================


The Next Step
===========================
We have just accomplished the "in-lab" calibration. Now we must tackle the "post-facto" calibration. It is essentially
trying to accomplish the same goal as the "in-lab" calibration, except we don't know the real position values of the
x ray hits. We must guess or estimate the parameters that we must use for our sigmoid function to transform our naive
position values back into the real position values, without knowing what the real values are or what the distortion is. 

Method
===========================
This can be done by using a well defined metric and using a genetic algorithm. We must choose a metric that once optimized
or maximized, will result in the parameters, and in result, the estimated real positions getting closer and closer to the actual
real values as possible. The genetic algorithm will help us perform the optimziation or maximization of said metric

Metric
===========================
The metric we ultimately ended up using is the normalized difference between the highest peak, and the second highest peak
in the cross correlation between the readout and mask. This is because when there is little to no distortion, there is a clear
highest peak in the cross correlation. However, when mid to high distortion is present, competing peaks start to rise, providing
ambiguity as to which peak is the highest

.. image:: images/cross-correlation-peaks.png 


Putting it all Together
===========================
Now its time to start translating this all to code. Our postfactolib library already implements the genetic algorithm and 
our chosen metric. Lets get our actual and distorted positions, pass them to our genetic agorithm, optimize our metric, and
generate some parameters. Then lets plot our results

.. code-block:: python 

    #Import everything we need
    import matplotlib.pyplot as plt
    import sys
    import numpy as np
    import random

    import postfactolib as pfl
    from postfactolib import distort, calibration_none, calibration_sigmoid, calibration_logistic, calc_metric


    verbose = False
    # some parameters of the run -- based on proportional counter on HETE-2
    n_photons = 250
    noise_fraction = 0.2
    distortion_dial = 0.8        # limits are [0, 1]
    theta_deg = 25

    # random.seed(1234)               # FIXME: for debugging only

    def main():

        #Our calibration function
        cal_func = calibration_logistic
        #Our guesses at the parameters
        cal_guess = [1, 0, 1, 0]

        # Generate our position lists.  note that we will never use
        # poslist_calibrated, since we will be experimening with many
        # different calibration functions

        poslist_real, poslist_distorted, _ \
            = pfl.sim_photonlists(cal_func, cal_guess, n_photons, distort_dial=distortion_dial)

        # now use our GA to find optimal parameters
        params, metric, sol_idx = pfl.calibrate_by_search(cal_func, noise_fraction, theta_deg, poslist_distorted)

        #Print our found parameters
        print('params from GA:', params)
        pfl.plot_calibration_panel(cal_func, params, poslist_real, poslist_distorted, metric, theta_deg, noise_fraction, distortion_dial)

Now lets take a look at what we've got

Something Interesting Arises
==============================
Here's an expected result we would get from using our calibration method 

.. image:: images/expected-result.png

The first graph is our mask and the second graph is our distorted readout. We're used to those, nothing new. 
The third graph is the readout generated after calibrating the distorted positions. The first cross correlation graph is 
between the distorted readout and the mask. The second is between the calibrated readout and the mask. The third is between 
the mask and the readout if there was no distortion present. In the last graph, the orange dot plot is our estimated positions and
the blue dot plot is our actual positions. We want the orange to look like the blue. Since the orange roughly lines up with the blue 
here, it means our calibration method works! We are able to fix distortion. However, something intriguing appears when you run
this multiple times

.. image:: images/intriguing-result.png

What's so interesting about these results? First thing one would notice is that our estimated positions are way off. Uh oh. However, the
intriguing aspect is in the cross correlation graphs. For now, only look at the distorted and calibrated cross correlations. The distorted 
cross correlation doesn't have one clear obvious peak. There are numerous competing peaks. Through our calibration method, those peaks get 
smudged down, resulting in a cross correlation with one single highest peak. That's good, that means our calibration method fixes distortion. 
So what though? Didn't we already establish that it did? 

Now take a look at the original (no distortion) and calibrated cross correlations. What do you notice? The original cross correlation has numerous 
competing peaks, and lacks one clear single highest peak. However, our calibrated readout *does* have one single high peak. Our calibrated cross correlation 
looks *better* than the original readout!

This brings up the question, what if our method is *over* calibrating? What if its going above and beyond, not only correcting our distortion, but other 
imperfections as well? One such imperfection that we have on hand is noise. Lets introduce noise and see what happens

.. image:: images/noise-calib-test.png

Amazing. Even with noise, our calibration method is able to produce a better cross correlation than not only the distorted 
readout, but the actual readout as well. If you take a look at the metric, our calibrated metric is higher than both. In both the 
distorted and actual cross correlations, there is no clear highest peak. However there is one in our calibrated cross correlation. 

Conclusion
===========
Through this research, we developed a calibration method that not only corrects our distortion, 
but also other imperfections as well. In future studies, further scrutiny into what those other forms of imperfection 
that our calibration method corrects and why our calibration method is able to correct them would be very valuable to 
look into 
