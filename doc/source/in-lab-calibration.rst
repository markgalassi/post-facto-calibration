====================
 In-lab Calibration
====================

The Real Experimental Setup
===========================

Normally in a real physical laboratory, researchers would beam x rays
towards the detector, keep track of the actual physical location that
the x ray hit, then look at the measured location that the
proportional counter reads. This gives you a list of the real
positions that the x rays hit, and distorted (or instrument measured) positions. 
Researchers then would graph or map real to distorted measurments, essentially 
creating a scatterplot. By curve fitting that data with a chosen function, 
they can get the parameters that can be used to transform those distorted positions 
back into the real positions (essentially fixing the distortion)

Types of distortion functions
=============================

The underlying distortion function maps :math:`[0, 1] \rightarrow [0,
1]` bijectively.  A simple example could be:

.. math::

   {\rm distort}(x) = x + c a \sin(2\pi x)

where :math:`c` is a *distortion dial* with 0 being minimum
distortion, and 1 being maximum distortion.  The :math:`a` parameter
needs to be set so that the function stays bijective even at :math:`c
= 1`.  You can guarantee that by setting:

.. math::

   \frac{d ({\rm distort}(x))}{dx} > 0

which implies that:

.. math::

   1 + 2\pi a \cos(2\pi x) > 0 \implies a < \frac{1}{2\pi}

so we end up with:

.. math::

   {\rm distort}(x) = x + \frac{1}{2\pi} \sin(2\pi x)

Other possible functions to look at:

.. math::

   \; & \frac{x^{1/4} + x^4}{2} \\
   \; & \frac{\tanh(0.5*(-5*c + 10*x*c)) + 1}{2} \\
   \; & \left( x + \frac{1}{2\pi} \sin(2\pi x) \right) * \frac{\tanh(0.5*(-5*c + 10*x*c)) + 1}{2} \\

The distortion function we ultimately ended up going with was:

.. math::

   {\rm distort}(x) = \frac {(x^{0.25} + x^{9})}{2 - x} * c + x



The Faux-Experimental Setup
===========================

Fortunately, our simulation is all virtual. No need to shine physical
x rays onto a real detector.

What we need are 'real x-ray positions' and 'instrument-measured x-ray
positions.' When real-world imperfections causes distortion on our
'real x-ray positions,' we get our distorted 'instument-measured x-ray
positions.'  Using the postfactolib module, we can easily get those
values and curve fit them. First lets get those positions

.. code-block:: python

   #import everything we need
   import numpy as np
   from scipy.optimize import curve_fit
   import matplotlib.pyplot as plt

   #Import our postfactolib module/library and functions within it
   from postfactolib import distort, calibration_logistic, calibration_none
   import postfactolib as pfl

   # some parameters of the run -- Based on the measurements of the
   # proportional counter on the HETE-2
   n_photons = 1000
   noise_fraction = 0.0            # for calibration always use 0 noise
   distortion = 0.5                # limits are [0, 1]. Change to decrease or increase distortion
   theta_deg = 0                   # incidence angle, 0 if perpendicular

   def main():
      # Simulate photons hitting our detector, we can then fetch our
      # real and distorted positions
      det_poslist_real, det_poslist_distorted, _ = pfl.sim_photonlists(calibration_none, [], n_photons,
                                                                     distort_dial=distortion, verbose=False)

   if __name__ == '__main__':
      main()


Curve Fitting
=============
We now have our distorted and real position values (we don't need our calibrated positions). 
The next step is curve fitting. The mapping of real to distorted values tends to make an s-curve or sigmoid shape, so 
using a sigmoid like function should hopefully yield the best fit

We decided to curve fit with this calibration function

.. math::

   P_{\rm calibrated}(x) = \frac{L}{1 + e^{-k (x - x_0)}} + b

The library scipy allows us to curve fit 2 sets of data with a given function while also giving us the parameters it used
to do so. Perfect! The postfactolib module already has everything imported for us, and already performs the curvefit. Our 
distortion and calibration functions work on the range of [0:1], so we must scale our position values to be within that range.
After we get our curve fit, lets plot our results and print our parameters

.. code-block:: python

   #import everything we need
   import numpy as np
   from scipy.optimize import curve_fit
   import matplotlib.pyplot as plt

   #Import our postfactolib module/library and functions within it
   from postfactolib import distort, calibration_logistic, calibration_none
   import postfactolib as pfl

   # some parameters of the run -- Based on the measurements of the
   # proportional counter on the HETE-2
   n_photons = 1000
   noise_fraction = 0.0            # for calibration always use 0 noise
   distortion = 0.5                # limits are [0, 1]. Change to decrease or increase distortion
   theta_deg = 0                   # incidence angle, 0 if perpendicular

   def main():
      # Simulate photons hitting our detector, we can then fetch our
      # real and distorted positions
      det_poslist_real, det_poslist_distorted, _ = pfl.sim_photonlists(calibration_none, [], n_photons,
                                                                     distort_dial=distortion, verbose=False)

      #We are curve fitting with our logistic function
      cal_func = calibration_logistic

      #Initial guesses for our curvefit parameter list
      guess = [1, 0, 1, 0]

      #Use our function and fetch the parameters after the curve fit
      params, pcov = fit_cal_data(det_poslist_distorted, det_poslist_real,
                                 cal_func, guess)

      #Print our parameters
      print(params)

      # now lets see how the calibration looks
      pfl.plot_cal_diagnostics(det_poslist_real, det_poslist_distorted, cal_func, params, distortion)

   
   def fit_cal_data(poslist_distorted, poslist_real, cal_func, p0):
      """Calculates the curve fit using scipy's curve_fit() method.  First
      converts all data to the [0, 1] range, then invokes scipy with our
      calibration function, and returns the calibration parameters and
      the covariance on those.
      """

      dist_01range = (np.array(poslist_distorted) - pfl.mask_det_offset_cm) / pfl.det_width_cm
      real_01range = (np.array(poslist_real) - pfl.mask_det_offset_cm) / pfl.det_width_cm
   
      params, pcov = curve_fit(cal_func, dist_01range, real_01range, p0)

      return params, pcov

      
   if __name__ == '__main__':
      main()


Results
=============
After running the above program, some plots should be presented before us. 

.. image:: images/do-in-lab-image.png

Our first plot in the top left shows us the curve of our calibration function.
The plot in the top right shows us the curve of our distortion function. The plot in the bottom left shows the distortion function being corrected by our 
calibration function, turning it into as straight of a line as it can. This means that our calibration function is indeed fixing distortion! 
The bottom right graph shows our distorted positions mapped to our actual positions, and as you can see, it matches the curve of our calibration function, 
meaning our calibration function curve fits correctly.  If we take a look at our terminal, we should see the parameters used for the sigmoid curve fit. 

.. image:: images/parameter-image.png

We have now successfully performed an "in-lab" calibration of our virtual proportional counter!

*The above program can be found in the "codedaperture" folder. It is titled "do-in-lab-calibration.py"*
