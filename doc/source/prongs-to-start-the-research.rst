==============================
 Prongs to start the research
==============================

This is my (markgalassi) suggestion as we start:

Take an approach that does two things simultaneously:

Learn background topics:
   * Image quality
   * Point spread function
   * Types of distortion for optical images
   * Coded aperture images
   * Optimization techniques (hill climbing, genetic algorithms,
     simulated annealing, ...)

Start writing code
   * Model a set of data that you would get from a coded aperture
   * Model a picture that you would get from a CCD
   * Take a rather nice picture, and apply distortion to it
   * Take good and distorted images and apply the metric

After this the project should have good momentum.
