#! /usr/bin/env python3

#import our module
import argparse
import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

from postfactolib import distort, calibration_none, calibration_logistic
import postfactolib as pfl

# some parameters of the run -- Based on the measurements of the
# proportional counter on the HETE-2
n_photons = 1000
noise_fraction = 0.0            # for calibration always use 0 noise
distortion = 0.5                # limits are [0, 1]. Change to decrease or increase distortion
theta_deg = 0                   # incidence angle, 0 if perpendicular

parser = argparse.ArgumentParser()
parser.add_argument('n_photons', help='Number of photons (Integer)')
parser.add_argument('theta_deg', help='Incidence Angle (Integer)')
parser.add_argument('distortion', help='Distortion Value (Range from 0-1). Will crash if 0')
parser.add_argument('noise', help='Noise Value (Range from 0-1)')

args = parser.parse_args()

def main():
    # Simulate photons hitting our detector, we can then fetch our
    # real and naive positions
    det_poslist_real, det_poslist_distorted, _ = pfl.sim_photonlists(pfl.calibration_none,
                                                                 [],
                                                                 int(args.n_photons),
                                                                 distort_dial=float(args.distortion),
                                                                 verbose=False)

    
    cal_func = calibration_logistic
    #Initial guesses for our curvefit parameter list
    guess = [1, 0, 1, 0]
 
    params, pcov = fit_cal_data(det_poslist_distorted, det_poslist_real,
                                cal_func, guess)

    print(params)
    # now lets see how the calibration looks
    pfl.plot_cal_diagnostics(det_poslist_real, det_poslist_distorted, cal_func, params, distortion)


def fit_cal_data(poslist_distorted, poslist_real, cal_func, p0):
    """Calculates the curve fit using scipy's curve_fit() method.  First
    converts all data to the [0, 1] range, then invokes scipy with our
    calibration function, and returns the calibration parameters and
    the covariance on those.
    """
    #print('p0:', p0)

    dist_01range = (np.array(poslist_distorted) - pfl.mask_det_offset_cm) / pfl.det_width_cm
    real_01range = (np.array(poslist_real) - pfl.mask_det_offset_cm) / pfl.det_width_cm
   
    params, pcov = curve_fit(cal_func, dist_01range, real_01range, p0)
                          
    return params, pcov


if __name__ == '__main__':
    main()
