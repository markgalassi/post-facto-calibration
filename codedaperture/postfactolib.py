import numpy as np
import matplotlib.pyplot as plt
import math
from math import sin, pi
import random
from collections import OrderedDict
from scipy.optimize import curve_fit
from scipy import signal
import pygad

verbose = False

# the mask is static of some 70 or 72 zeros and ones. zero means the
# mask is closed, 1 means the mask is open.

# mask_real is something we feel is the real mask
mask_real = [1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1,
             0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0,
             1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0,
             1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1]

# some parameters of the run -- based on the proportional counter on the HETE-2

mask = mask_real
det_height_cm = 18.74
mask_width_cm = 20.8
det_width_cm = 12.0
det_pix_size_cm = 0.0495
n_det_pix = int(mask_width_cm / det_pix_size_cm + 0.5) # +0.5? or +1? or +0?
mask_det_offset_cm = (mask_width_cm - det_width_cm) / 2


#Paramaters for our genetic algorithm
sol_per_pop = 50 #Number of solutions per population
num_genes = 4 #Number of genes (essentially parameters)
init_range_low = 0 #-max(det_poslist_naive) #Lowest value that a parameter can be
# init_range_high = mask_width_cm - mask_det_offset_cm #Highest value that a parameter can be
init_range_high = 1
mutation_percent_genes = 10 #Percent chance that a gene is mutated (increases variety)

def distort(x_good, distortion_dial):
    """Bare core of distortion function, mapping [0, 1] -> [0, 1], must be
    bijective.  Example: x -> x + c*0.1*sin(2*pi*x).  The
    distorion_dial goes from 0 (no disortion) to 1 (max distortion).
    """
    assert(x_good >= 0 and x_good <= 1)
    #x_bad = x_good + distortion_dial*0.1*sin(2*pi*x_good)
    x_bad = ((x_good**(0.25) + x_good**9) / 2 - x_good) * distortion_dial + x_good
    assert(x_bad >= 0 and x_bad <= 1)
    return x_bad

def apply_cal_on_det_pos(cal_func, params, det_pos_cm):
    """Apply the calibration function (which is usually defined on the
    [0,1] interval) to an actual measured value in cm.  This has to
    scale down and then scale back up after the transformation.
    """
    x_01range = (det_pos_cm - mask_det_offset_cm) / det_width_cm
    x_cal_01range = cal_func(x_01range, *params)
    x_cal = mask_det_offset_cm + x_cal_01range * det_width_cm
    return x_cal

def calibration_none(x_bad):
    """Function that does no calibration."""
    return x_bad

def calibration_parabola(x_bad, a, b, c):
    """Function that does no calibration."""
    return a*x_bad**2 + b*x_bad + c

def calibration_sigmoid(x_bad, a, b, c, d):#, param_set):
    """Applies the rational function y = a/(x-b) + c + d*x."""
    return (a / (x_bad - b)) + c + (d*x_bad)

def calibration_logistic(x_bad, L, x0, k, b): #, param_set):
    return L / (1 + np.exp(-k*(x_bad-x0))) + b

def calibration_esoteric(x_bad, a, b, c, d, e, f, g):
    """Applies a big polynomial to x_bad."""
    return a*x_bad**6 + b*x_bad**5 + c*x_bad**4 + d*x_bad**3 + e*x_bad**2 + f*x_bad + g

   
def calibrate_by_search(cal_func, noise_fraction, theta_deg, poslist_distorted):

    def fitness_function(ga, solution, solution_idx):
        """Fitness function for our genetic algorithm. Incorporates our own
        defined metric to create the metric that the genetic algorithm
        will use. The genetic algorithm will try to optimize our own
        defined metric and try to get it as close to the target metric
        as possible
        """
        #print(solution)
        calibrated_readout = np.zeros(n_det_pix)
        calibrated_poslist = []
        metric = 0
        if solution[2] > 0: # k is never negative
            #Create our calibrated positions based off of our distorted positions
            for i in range(len(poslist_distorted)):
                x_01range = (poslist_distorted[i] - mask_det_offset_cm) / det_width_cm
                calibrated_01range = cal_func(x_01range, solution[0], 
                                              solution[1], solution[2], solution[3])
                calibrated_pos = mask_det_offset_cm + calibrated_01range * det_width_cm
                calibrated_poslist.append(calibrated_pos)
            # Create a readout from our newly made calibrated positions
            calibrated_readout = photons2readout(calibrated_poslist, noise_fraction, theta_deg)
            metric = calc_metric(resampled_mask, calibrated_readout)
            # print('METRIC:', metric, solution)
        return metric
  
    # calibrated_readout = pfl.photons2readout(poslist_calibrated, noise_fraction, theta_deg)
    # to do cross-correlation we must resample the mask to have as
    # many pixels as the detector
    resampled_mask = resample_mask(n_det_pix)
  
    #With our new calibration function, it requires a good guess or else
    #our estimated data will be way off. Here we create an initial population for
    #the genetic algorithm to start with that just consists of those guesses
    #The guesses are in the format [L, x0, k, b]
    p0 = [1, 0, 1, 0]
    initial_pop = []
    for x in range(0, 50):
        initial_pop.append(p0)
    #Create our genetic algorithm object
    ga = pygad.GA(num_generations=200,
                       num_parents_mating=2, 
                       fitness_func=fitness_function,
                       sol_per_pop=sol_per_pop, 
                       num_genes=num_genes,
                       init_range_low=init_range_low,
                       init_range_high=init_range_high,
                       mutation_percent_genes=mutation_percent_genes,
                       mutation_num_genes=4,
                       crossover_probability=0.25,
                       allow_duplicate_genes=True,
                       initial_population=initial_pop)
   
    
    #Run the genetic algorithm
    ga.run()
    # Once we get our solution (our parameters), use the parameters to
    # transform the distorted positions into an estimate of the real
    # positions
    solution, solution_fitness, solution_idx = ga.best_solution()

    return solution, solution_fitness, solution_idx

def plot_cal_diagnostics(det_poslist_real, det_poslist_distorted, cal_func, params, distortion):
    """Try to visualize what has happend with calibration, using a panel
    of plots."""
    xrange = np.linspace(0, 1, 100)
    yvals = np.array([cal_func(x, *params) for x in xrange])

    figure, ax = plt.subplots(2, 2, constrained_layout=True, figsize=(15, 10))

    # print(xrange, yvals)
    ax[0][0].axes.plot(xrange, yvals)
    ax[0][0].axes.set_ylabel('calibration curve')
    ax[0][0].axes.set_xlabel('det_pos [0,1] scale')

    ydistort = np.array([distort(x, distortion) for x in xrange])
    ax[0][1].axes.plot(xrange, ydistort)
    ax[0][1].axes.set_ylabel('distortion function')
    ax[0][1].axes.set_xlabel('det_pos [0,1] scale')

    yrecovered = np.array([cal_func(x, *params) for x in ydistort])
    ax[1][0].axes.plot(xrange, yrecovered)
    ax[1][0].axes.set_ylabel('recovered x after calibration')
    ax[1][0].axes.set_xlabel('det_pos [0,1] scale')

    ax[1][1].axes.scatter(det_poslist_distorted, det_poslist_real)
    ax[1][1].axes.set_ylabel('Real positions')
    ax[1][1].axes.set_xlabel('Distorted positions')

    #Code if you want to save the plots
    '''
    cal_func_name = cal_func.__str__().split()[1]
    fname = (f'cal_diag_{cal_func_name}_distortion{distortion}_theta{theta_deg}'
             + f'_noise_{noise_fraction}.png')
    plt.savefig(fname})
    print(f'# wrote plot to {fname}')
    '''
    plt.show()


def plot_calibration_panel(cal_func, params, poslist_real, poslist_distorted, calibrated_metric, theta_deg, noise_fraction, distortion_dial):
    
    #Create our estimated position list using our newly found parameters
    estimated = []
    for x in range(len(poslist_distorted)):
        estimated_pos = apply_cal_on_det_pos(cal_func, params, poslist_distorted[x])
        if (estimated_pos > mask_det_offset_cm and estimated_pos < (mask_width_cm - mask_det_offset_cm)):
            estimated.append(estimated_pos)
    
    #Create our readout from our estimated position list
    real_readout = photons2readout(poslist_real, noise_fraction, theta_deg)
    distorted_readout = photons2readout(poslist_distorted, noise_fraction, theta_deg)
    calibrated_readout = photons2readout(estimated, noise_fraction, theta_deg)
    #Cross correlate our readouts and our mask
    resampled_mask = resample_mask(n_det_pix)
    ccor_mask_calibrated = np.correlate(resampled_mask, calibrated_readout, 'full')
    ccor_no_distortion = np.correlate(resampled_mask, real_readout, 'full')
    ccor_maskreadout = np.correlate(resampled_mask, distorted_readout, mode='full')
    #Stuff to get the lag 
    lag = ccor_maskreadout.argmax() - (len(resampled_mask) - 1)
    metric_no_distortion = calc_metric(resampled_mask, real_readout)
    distorted_metric = calc_metric(resampled_mask, distorted_readout)
   
    # finally we can plot what we've done
  
    plot_calibration(distorted_readout, estimated, lag, theta_deg, metric_no_distortion, calibrated_metric, poslist_distorted, 
                  poslist_real, ccor_maskreadout, ccor_mask_calibrated, calibrated_readout, ccor_no_distortion, distorted_metric, distortion_dial)

def plot_calibration(det_readout, estimated, lag, theta_deg, original_metric, generated_metric, 
                  naive_vals, real_vals, ccor_maskreadout, ccor_mask_calibrated, calibrated_readout, ccor_pure, distorted_metric, distortion):
    """At this time all the plotting stuff is shoved here"""
    
    # X axes of graphs
    resampled_mask = resample_mask(n_det_pix)
    xaxisForResampledMask = np.arange(0, len(resampled_mask), 1)
    xaxisForCount = np.arange(0, len(det_readout), 1)
    xaxis_maskreadout = np.arange(0, len(ccor_maskreadout), 1) - len(ccor_maskreadout) // 2
    xaxis_calibrated = np.arange(0, len(ccor_mask_calibrated), 1) - len(ccor_mask_calibrated) // 2
    
    # Everything after this is plotting our graphs
    figure, ax = plt.subplots(7, constrained_layout=True, figsize=(15, 10))
    ax[0].axes.yaxis.set_ticklabels([])
    #ax[1].axes.yaxis.set_ticklabels([])
    figure.set_figheight(14)
    figure.set_figwidth(25)
    figure.suptitle(f'Mask/readout cross-correlations, theta={theta_deg} deg, distortion={distortion}')
    
    ax[0].bar(xaxisForResampledMask, ((np.zeros(len(resampled_mask)) + 1) - resampled_mask),
              width=1.0, align='edge', color='black', edgecolor='white')
    ax[0].bar(xaxisForResampledMask, resampled_mask,
              width=1.0, align='edge', color='orange', edgecolor='white', alpha=0.8)
    ax[0].set_title('resampled mask (solid black is closed, orange means open)')
    ax[1].bar(xaxisForCount, det_readout)
    ax[1].set_title('detector readout (x-axis is in mask coordinate frame)')
    ax[2].bar(xaxisForCount, calibrated_readout)
    ax[2].set_title('calibrated detector readout (x-axis is in mask coordinate frame)')
    ax[3].bar(xaxis_maskreadout, ccor_maskreadout)
    ax[3].set_title(f'Cross Correlation of Mask and Distorted Readout - lag is {lag}')
    ax[4].bar(xaxis_calibrated, ccor_mask_calibrated, color='green')
    ax[4].set_title(f'Cross Correlation of Mask and Calibrated Readout')
    ax[5].bar(xaxis_calibrated, ccor_pure)
    ax[5].set_title(f'Cross Correlation of Mask and No Distortion Readout')
    yAxis = []
    for x in range(len(real_vals)):
        yAxis.append(0)
    yAxis_calibrated = []
    for x in range(len(estimated)):
        yAxis_calibrated.append(0.1)
    ax[6].scatter(real_vals, yAxis, label='Actual cm')
    ax[6].scatter(estimated, yAxis_calibrated, label = 'Estimated cm')
    #ax[6].scatter(naive_vals, yAxis, label = 'Distorted cm')
    ax[6].set_title(f'Distorted Metric is {distorted_metric} - Calibrated Metric is {generated_metric} - No Distortion Metric is {original_metric}')
    ax[6].legend(loc = 'lower right')
    plt.show()

def plot_xcor_triple(resampled_mask, ro1, ro2, ro3, save=None):
    """Plots cross correlation between resampled mask and detector readout"""
    # X axes of graphs
    xaxisForMask = np.arange(0, len(mask), 1)
    # xaxisForResampledMask = np.arange(0, mask_width_cm, mask_width_cm / len(resampled_mask))
    xaxisForResampledMask = np.arange(0, len(resampled_mask), 1)
    # xaxisForCount = np.arange(0, mask_width_cm, mask_width_cm / len(det_readout))

    # Everything after this is plotting our graphs
    figure, ax = plt.subplots(6, constrained_layout=True, figsize=(15, 10))
    ax[0].axes.yaxis.set_ticklabels([])
    ax[1].axes.yaxis.set_ticklabels([])

    figure.set_figheight(14)
    figure.set_figwidth(25)
    figure.suptitle(f'Mask/readout cross-correlations, theta=??? deg')
    # figure.suptitle(f'Mask/readout cross-correlations, theta={angle_deg} deg')

    ax[0].bar(xaxisForMask, ((np.zeros(len(mask)) + 1) - mask), width=1.0,
              align='edge', color='black', edgecolor='white')
    ax[0].bar(xaxisForMask, mask, width=1.0,
              align='edge', color='orange', edgecolor='white', alpha=0.8)
    ax[0].set_title('mask (solid black is closed, orange is open)')
    
    ax[1].bar(xaxisForResampledMask, ((np.zeros(len(resampled_mask)) + 1) - resampled_mask),
              width=1.0, align='edge', color='black', edgecolor='white')
    ax[1].bar(xaxisForResampledMask, resampled_mask,
              width=1.0, align='edge', color='orange', edgecolor='white', alpha=0.8)
    ax[1].set_title('resampled mask (solid black is closed, orange means open)')

    xaxisForCount = np.arange(0, len(ro1), 1)
    ax[2].bar(xaxisForCount, ro1)
    ax[2].set_title('detector readout (x-axis is in mask coordinate frame)')

    for i, readout in enumerate([ro1, ro2, ro3]):
        xcor = np.correlate(resampled_mask, readout, 'full')
        xaxiscc = np.arange(0, len(xcor), 1) - len(xcor) // 2
        ax[3+i].bar(xaxiscc, xcor, color='blue')
        ax[3+i].set_title(f'Cross Correlation (bars) - lag is ???')

        # align.xaxes(ax[1], mask_width_cm/2, ax[2], mask_width_cm/2, 0.5)

    if save:
        for suffix in ['png', 'svg']:
            ofname = f'triple.{suffix}'
            print(f'saving file {ofname}')
            plt.savefig(ofname)
    # plt.show()
    return plt

def calc_metric(mask, estimated_readout):#, mean):
        """Generates our metric"""
        
        #Cross correlate the mask and whatever array is passed through
        xcor = np.correlate(mask, estimated_readout, 'full')

        #Get the mean of the cross correlation
        xcor_count = 0
        for x in xcor:
            xcor_count += x
        mean = xcor_count / len(xcor)

        highest = 0
        second_highest = 0

        if(np.all((xcor == 0))):
            return 0

        else:
            #Find the index of the peaks
            peak_indices, peak_dict = signal.find_peaks(xcor, height=0.2, distance=15)
            peak_heights = peak_dict['peak_heights']
            

            highest_peak_index = peak_indices[np.argmax(peak_heights)]
            second_highest_peak_index = peak_indices[np.argpartition(peak_heights,-2)[-2]]
            third_highest_peak_index = peak_indices[np.argpartition(peak_heights, -3)[-3]]
            fourth_highest_peak_index = peak_indices[np.argpartition(peak_heights, -4)[-4]]

            #Get values of peaks
            highest = xcor[highest_peak_index]
            second_highest = xcor[second_highest_peak_index]
            third_highest = xcor[third_highest_peak_index]
            fourth_highest = xcor[fourth_highest_peak_index]
        
        # Calculate our metric, based on the difference of highest
        # peaks, though we could also try the ratio.  Use a
        # normalization of xcor_count, although we could also try
        # using the mean
        if verbose:
            print('highest, second, ...', highest, second_highest, third_highest, fourth_highest)
        metric_ratio = highest / second_highest
        metric_diff_norm_highest = 10*(highest - second_highest) / highest
        metric_diff_norm_mean = 1000*(highest - second_highest) / mean
        metric_diff_norm_counts = 1000*(highest - second_highest) / xcor_count
        if verbose:
            print('metrics_ratio_diffnormmean_diffnormcounts', metric_ratio, metric_diff_norm_highest,
                  metric_diff_norm_mean, metric_diff_norm_counts, 1000*highest / xcor_count, xcor_count)
        return metric_diff_norm_highest

def resample_mask(n_det_pix):
    """Resamples the mask based on the length of the detector readout"""
    #print(n_det_pix)
    resampled_mask = np.zeros(n_det_pix)
    for resampled_ind in range(n_det_pix):
        mask_ind = int(resampled_ind * len(mask_real) / n_det_pix)
        val = mask_real[mask_ind]
        resampled_mask[resampled_ind] = val

    return resampled_mask

def mask_pos_angle_consistent(det_pos, angle_deg, verbose=False):
    """Determines if the mask and detector position pairs are compatible
    with a photon incident at angle_deg
    """
    # to do this geometry, first figure out the position under the
    # mask if that photon had been straight
    perp_pos_cm = det_pos + det_height_cm * math.tan(angle_deg * math.pi / 180.0)
    if verbose:
        print('perp_pos_cm:', perp_pos_cm)

    # now find the mask slit index, so we can see if it is open or closed
    mask_slit_pos_cm = perp_pos_cm
    mask_slit_ind = int((mask_slit_pos_cm / mask_width_cm) * len(mask_real))
    if verbose:
        print('MASK_offset,perp,slitpos,width,ratio,slit_ind,len',
              mask_det_offset_cm, perp_pos_cm, 
              mask_slit_pos_cm, mask_width_cm, (mask_slit_pos_cm / mask_width_cm),
              mask_slit_ind, len(mask_real))
    if mask_slit_ind < 0 or mask_slit_ind >= len(mask_real):
        return False
    assert(mask_slit_ind >= 0 and mask_slit_ind < len(mask_real))
    valid = (mask_real[mask_slit_ind] == 1)
    if verbose:
        print(det_pos, angle_deg, valid)
    return valid

def sim_photonlists(calibration_func, cal_params, n_photons, distort_dial=0, verbose=False):
    """Simulate photons hitting the detector through the mask and at the
    set angle.  Returns three lists: the photon real positions,
    the ditorted positions, and the distorted-and-then-calibrated
    positions.
    """
    assert(type(cal_params) == type([]))
    # cal_params = [0, 0, 0, 0]

    #print('sim_photons:', calibration_func.__str__().split()[1], distort_dial)
    real_poslist = []
    distort_poslist = []
    calib_poslist = []
    # simulate a bunch of photons hitting the detector
    for i in range(n_photons):
        # hypothetical position where it might have hit.  note that we
        # measure things starting at the left end of the mask, and the
        # detector valid range is smaller than that, so we must do
        # arithmetic with those offsets to generate a valid detector
        # position
        det_pos_cm = mask_det_offset_cm + random.random() * det_width_cm
        real_poslist.append(det_pos_cm)
        distorted_pos_cm = apply_distortion(det_pos_cm, distort_dial)
        distort_poslist.append(distorted_pos_cm)
        recalibrated_pos_cm = calibration_func(distorted_pos_cm, *cal_params)
        calib_poslist.append(recalibrated_pos_cm)
    return real_poslist, distort_poslist, calib_poslist
            

def photons2readout(pos_list, noise, angle_deg):
    """Takes a list of photons and makes a detector readout compatible
    with the mask shadow."""
    # Contains our extrapolated mask
    readout = np.zeros(n_det_pix)
    # first sanity check: is the detector position < 0 or > width?
    # i.e. is it out of bounds?
    for det_pos_cm in pos_list:
        # assert(det_pos_cm >= mask_det_offset_cm
        #        and det_pos_cm < mask_det_offset_cm + det_width_cm)
        # now see if that position is consistent with the
        # mask and the angle it's coming from
        if random.random() < noise:
            is_valid = True     # noise is always valid
        else:
            is_valid = mask_pos_angle_consistent(det_pos_cm, angle_deg, False)
        if is_valid:
            readout_ind = int(n_det_pix * det_pos_cm / mask_width_cm)
            # assert(readout_ind >= 0 and readout_ind < n_det_pix)
            # if readout_ind < 0:
            #     readout_ind = 0
            # if readout_ind >= len(readout):
            #     readout_ind = len(readout) - 1
            if readout_ind < 0 or readout_ind >= len(readout):
                continue              # ignore out-of-bounds counts
            readout[readout_ind] += 1 # yay! I got a count on the detector
    #print(readout)
    return readout
        

def apply_distortion(x_real, distortion_dial=0):
    """Applies a simple distortion to the signal -- in this case something
    like x -> x + c*sin(2*pi*x / scale)
    """
    #print('ENTER apply_distortion')
    x_01range = (x_real - mask_det_offset_cm) / det_width_cm
    assert(x_01range >= 0 and x_01range <= 1)
    x_distorted = distort(x_01range, distortion_dial)
    x_bad = mask_det_offset_cm + x_distorted * det_width_cm
    #print(distortion_dial, x_01range, ' --> ', x_distorted)
    #print(distortion_dial, x_real, ' --> ', x_bad)
    return x_bad


def plot_xcor(resampled_mask, det_readout, ccor, angle_deg, lag=None, save=None):
    """Plots cross correlation between resampled mask and detector readout"""
    # X axes of graphs
    xaxisForMask = np.arange(0, len(mask_real), 1)
    # xaxisForResampledMask = np.arange(0, mask_width_cm, mask_width_cm / len(resampled_mask))
    xaxisForResampledMask = np.arange(0, len(resampled_mask), 1)
    # xaxisForCount = np.arange(0, mask_width_cm, mask_width_cm / len(det_readout))
    xaxisForCount = np.arange(0, len(det_readout), 1)
    xaxiscc = np.arange(0, len(ccor), 1) - len(ccor) // 2

    # Everything after this is plotting our graphs
    figure, ax = plt.subplots(4, constrained_layout=True, figsize=(15, 10))
    ax[0].axes.yaxis.set_ticklabels([])
    ax[1].axes.yaxis.set_ticklabels([])
        
    figure.set_figheight(14)
    figure.set_figwidth(25)
    figure.suptitle(f'Mask/readout cross-correlations, theta={angle_deg} deg')
    
    ax[0].bar(xaxisForMask, ((np.zeros(len(mask_real)) + 1) - mask_real), width=1.0,
              align='edge', color='black', edgecolor='white')
    ax[0].bar(xaxisForMask, mask_real, width=1.0,
              align='edge', color='orange', edgecolor='white', alpha=0.8)
    ax[0].set_title('mask (solid black is closed, orange is open)')
    
    ax[1].bar(xaxisForResampledMask, ((np.zeros(len(resampled_mask)) + 1) - resampled_mask),
              width=1.0, align='edge', color='black', edgecolor='white')
    ax[1].bar(xaxisForResampledMask, resampled_mask,
              width=1.0, align='edge', color='orange', edgecolor='white', alpha=0.8)
    ax[1].set_title('resampled mask (solid black is closed, orange means open)')

    ax[2].bar(xaxisForCount, det_readout)
    ax[2].set_title('detector readout (x-axis is in mask coordinate frame)')

    # OK, I confess I'm not sure how "lag" is measured
    ax[3].bar(xaxiscc, ccor, color='blue')
    ax[3].set_title(f'Cross Correlation (bars) - lag is {lag}')

    # align.xaxes(ax[1], mask_width_cm/2, ax[2], mask_width_cm/2, 0.5)

    if save:
        for suffix in ['png', 'svg']:
            ofname = f'mask_readout_ccor.{suffix}'
            print(f'saving file {ofname}')
            plt.savefig(ofname)
    # plt.show()
    return plt

def plot_curvefit(distorted_pos, real_pos, calibration_func, param_list, fit=None, verbose=None):
    """Uses scipy to plot our data and curve fitting lines"""
    if fit is None:
        fit = 'all'

    #create dictionary mapping x and y values (x-> key, y->value)
    count = 0
    dict = {}
    for dpos in (distorted_pos):
        dict[dpos] = real_pos[count]
        count += 1
        
    #Sort dictionary by x so that all of our data is in order. Store keys to x and values to y
    dictSorted = OrderedDict(sorted(dict.items()))

    #x and y for our scipy functions
    x = np.array(list (dictSorted.keys()) )
    y = np.array(list (dictSorted.values() ))

    p0 = param_list
    print('p0:', p0)
    params, _ = curve_fit(calibration_func, x, y, p0, bounds=(-max(y), mask_width_cm - mask_det_offset_cm),
                          method='trf')
    print(params)
  
    return plt

# def set_theta(angle):
#     """Sets the theta angle. Used for animating plots"""
#     angle_deg = angle

# def set_noise(noise_val):
#     """Sets the noise value. Used for animating plots"""
#     noise = noise_val

def animate(range_vals, mode, n_photons, distortion=0):
    """Plots cross correlation but animates either the change in angle or change in noise"""
    figure, ax = plt.subplots(4, constrained_layout=True, figsize=(20, 10))
        
    if mode == 'theta':
        range_nums = range(range_vals[0], range_vals[1])
    if mode == 'noise':
        range_nums = [x * 0.1 for x in range(range_vals[0], range_vals[1])]

    for num in range_nums: #Starts at -1 because the first angle is not animated due to slow run time. If start at 0, it'll show 1
        if mode =='theta':
            theta_deg = num
        if mode == 'noise':
            noise = num

        # find the detector readout for a given angle
        det_poslist_real, det_poslist_distorted, _ = sim_photonlists(calibration_none, [], n_photons,
                                                                     distort_dial=distortion, verbose=False)
        det_readout = photons2readout(det_poslist_distorted, noise, theta_deg,)
        resampled_mask = resample_mask(n_det_pix)
        
        # Our cross correlation array
        # ccor = np.correlate(resampled_mask, det_readout, mode='full')
        ccor = np.correlate(resampled_mask, det_readout, mode='same')
           
        # trick to get the lag
        lag = ccor.argmax() - (len(resampled_mask) - 1)

        #Plot our data
        # X axes of graphs
        xaxisForMask = np.arange(0, len(mask_real), 1)
        # xaxisForResampledMask = np.arange(0, mask_width_cm, mask_width_cm / len(resampled_mask))
        xaxisForResampledMask = np.arange(0, len(resampled_mask), 1)
        # xaxisForCount = np.arange(0, mask_width_cm, mask_width_cm / len(det_readout))
        xaxisForCount = np.arange(0, len(det_readout), 1)
        xaxiscc = np.arange(0, len(ccor), 1) - len(ccor) // 2
            
        # Everything after this is plotting our graphs
            
        #Clears our changing plots
        ax[2].clear()
        ax[3].clear()
         
        ax[0].axes.yaxis.set_ticklabels([])
        ax[1].axes.yaxis.set_ticklabels([])

        #figure.set_figheight(14)
        #figure.set_figwidth(25)
        if mode == 'theta':
            figure.suptitle(f'Mask/readout cross-correlations, theta={num} deg')
            
        if mode == 'noise':
            figure.suptitle(f'Mask/readout cross-correlations, noise={num}')

        ax[0].bar(xaxisForMask, ((np.zeros(len(mask_real)) + 1) - mask_real), width=1.0,
                  align='edge', color='black', edgecolor='white')
        ax[0].bar(xaxisForMask, mask_real, width=1.0,
                  align='edge', color='orange', edgecolor='white', alpha=0.8)
        ax[0].set_title('mask (solid black is closed, orange is open)')
        
        ax[1].bar(xaxisForResampledMask, ((np.zeros(len(resampled_mask)) + 1) - resampled_mask),
                  width=1.0, align='edge', color='black', edgecolor='white')
        ax[1].bar(xaxisForResampledMask, resampled_mask,
                  width=1.0, align='edge', color='orange', edgecolor='white', alpha=0.8)
        ax[1].set_title('resampled mask (solid black is closed, orange means open)')

        ax[2].bar(xaxisForCount, det_readout)
        ax[2].set_title('detector readout (x-axis is in mask coordinate frame)')

        # OK, I confess I'm not sure how "lag" is measured
        ax[3].bar(xaxiscc, ccor, color='green')
        ax[3].set_title(f'Cross Correlation (bars) - lag is {lag}')

        figure.canvas.draw_idle()
        plt.pause(0.1)
