for distort in `seq 0 0.01 1`
do
    ./simphotons.py 10000 28 $distort 0.2
done
ffmpeg -y -framerate 3 -pattern_type glob -i 'xcor_n010000_c?.??_ang48.000_noise0.200.png' -i bach_paternoster-cello_suite-1.mp3 -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p sweep_distortion.mp4

for noise in `seq 0 0.01 1`
do
    ./simphotons.py 10000 12 0.5 $noise
done
ffmpeg -y -framerate 3 -pattern_type glob -i 'xcor_n010000_c0.50_ang12.000_noise?.???.png' -i janacek_kreutzer.mp3 -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p sweep_noise.mp4

for angle in `seq -57 0.1 57`
do
    ./simphotons.py 10000 $angle 0.5 0.1
done
ffmpeg -y -framerate 3 -pattern_type glob -i 'xcor_n010000_c0.50_ang*_noise0.100.png' -i ravel_sonate.ogg -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p sweep_angle.mp4

for n_photons in `seq 0 1000 100000`
do
    ./simphotons.py $n_photons 13 0.5 0.1
done
ffmpeg -y -framerate 3 -pattern_type glob -i 'xcor_n??????_c0.50_ang13.000_noise0.100.png' -i beethoven_bei_mannerm.mp3 -c:a copy -shortest -c:v libx264 -pix_fmt yuv420p sweep_n_photons.mp4
