# Coded Apertures

postfactolib.py is our library/module containing all functions used to create, plot, and optimize simulations related to coded apertures. Our module does rely on various other libraries. Install them if you have not already:

```
pip3 install numpy
pip3 install matplotlib
pip3 install math
pip3 install random
pip3 install collections
pip3 install scipy
pip3 install pygad
```

# Normal Simulation

"simphotons.py" simulates our coded mask and proportional counter. It plots our mask, the mask resampled to match the size of our detector read out, the detector read out (x ray counts), and a cross correlation between the mask and detector read out. The program takes 4 arguments. The number of photons you would like to simulate (Integer), the angle that those photons will be hitting the mask (Integer), and finally the amount of distortion and noise you would like (Range from 0-1). For help running the program run:
```
python3 simphotons.py -h 
```
To run a default simulation, say with 100,000 photons, an angle of 0, distortion of 0.9, and a noise value of 0, run:
```
python3 simphotons.py 100000 0 0.9 0
```

# In-Lab Calibration

"do-in-lab-calibration.py" simulates the in-lab curve fitting process used to find parameters in order to fix distortion. Take a look at "in-lab-calibration.rst" in the doc/source/ directory for more background information. For help running 
the program run:
```
python3 do-in-lab-calibration.py -h 
```

To run a default run, say with 100 photons, an angle of 10, distortion of 0.5, and a noise value of 0, run:
```
python3 do-in-lab-calibration.py 100 10 0.5 0
```

# Post-Facto Calibration

"calibrate.py" simulates the post facto calibration of distorted positions. Take a look at "post-facto-calibration.rst" in the doc/source/ directory for more background information. For help running the program run:
```
python3 calibrate.py -h
```

To run a default calibration, say with 200 photons, an angle of 10, distortion of 0.5, and noise of 0 run:
```
python3 calibrate.py 200 10 0.5 0
```
