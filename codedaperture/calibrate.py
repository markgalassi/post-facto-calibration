#! /usr/bin/env python3

import matplotlib.pyplot as plt
import sys
import numpy as np
import random
import argparse

import postfactolib as pfl
from postfactolib import distort, calibration_none, calibration_sigmoid, calibration_logistic, calc_metric


verbose = False
# some parameters of the run -- based on proportional counter on HETE-2
parser = argparse.ArgumentParser()
parser.add_argument('n_photons', help='Number of photons (Integer)')
parser.add_argument('theta_deg', help='Incidence Angle (Integer)')
parser.add_argument('distortion', help='Distortion Value (Range from 0-1)')
parser.add_argument('noise', help='Noise Value (Range from 0-1)')

args = parser.parse_args()

# random.seed(1234)               # FIXME: for debugging only

def main():
    print('Calibrating...')
    cal_func = calibration_logistic
    cal_guess = [1, 0, 1, 0]
    # Generate our position lists.  note that we will never use
    # poslist_calibrated, since we will be experimening with many
    # different calibration functions
    poslist_real, poslist_distorted, _ \
        = pfl.sim_photonlists(cal_func, cal_guess, int(args.n_photons), distort_dial=float(args.distortion))
    # now use a GA to find optimal parameters
    params, metric, sol_idx = pfl.calibrate_by_search(cal_func, float(args.noise), int(args.theta_deg), poslist_distorted)

    #Print our found parameters
    print('params from GA:', params)
    pfl.plot_calibration_panel(cal_func, params, poslist_real, poslist_distorted, metric, int(args.theta_deg), float(args.noise), float(args.distortion))


if __name__ == '__main__':
    main()
