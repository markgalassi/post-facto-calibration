#! /usr/bin/env python3

import argparse
import numpy as np
import postfactolib
# from postfactolib import plot_xcor_triple, calc_metric
import postfactolib as pfl

verbose = False

# random.seed(1234)               # FIXME: for debugging only

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--show', type=bool, help='Show plots live (apart from saving in file)')
    parser.add_argument('n_photons', help='Number of photons (Integer)')
    parser.add_argument('theta_deg', help='Incidence Angle (Integer)')
    parser.add_argument('distortion', help='Distortion Value (Range from 0-1)')
    parser.add_argument('noise', help='Noise Value (Range from 0-1)')

    args = parser.parse_args()

    # find the detector readout for a given angle
    real_poses, distort_poses, calib_poses = pfl.sim_photonlists(pfl.calibration_none,
                                                                 [],
                                                                 int(args.n_photons),
                                                                 distort_dial=float(args.distortion),
                                                                 verbose=False)
    real_readout = pfl.photons2readout(real_poses, float(args.noise), float(args.theta_deg))
    distort_readout = pfl.photons2readout(distort_poses, float(args.noise), float(args.theta_deg))
    calib_readout = pfl.photons2readout(calib_poses, float(args.noise), float(args.theta_deg))

    # to do cross-correlation we must resample the mask to have as
    # many pixels as the detector
    resampled_mask = pfl.resample_mask(len(distort_readout))

    # now that we have three readouts (real, distorted, distorted-and-corrected)
    # let's make three separate plots
    pfl.plot_xcor_triple(resampled_mask, real_readout, distort_readout, calib_readout,
                         save=True)

    print('Distorted Metric: ', pfl.calc_metric(resampled_mask, distort_readout))
    print('Real readout Metric: ', pfl.calc_metric(resampled_mask, real_readout))
    # # trick to get the lag
    # lag = xcor.argmax() - (len(resampled_mask) - 1)

    # # finally we can plot what we've done
    # plt = pfl.plot_xcor(resampled_mask, distort_readout, xcor, lag)
    # plot_fname = f'xcor_n{int(args.n_photons):06d}_c{float(args.distortion):.2f}_ang{float(args.theta_deg):.3f}_noise{float(args.noise):.3f}.png'
    # plt.savefig(plot_fname)
    # print(f'# saved image to {plot_fname}')


if __name__ == '__main__':
    main()
