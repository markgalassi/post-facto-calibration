#! /usr/bin/env python

"""
compute a variety of measures: autocorrelation, Hurst exponent, partial info content, info content, power spectrum, 
"""

import sys
import glob
import os
import re
from pprint import pprint
import string
import getopt

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np
import math
from matplotlib.mlab import griddata

from math import log
from math import sqrt 
import numpy as np
import scipy
from scipy.optimize import leastsq

colormap = cm.jet

def __MAIN__():
    outdir = sys.argv[1]

    timeline_fnames = glob.glob(outdir + '/*timeline_incr')
    timeline_fnames.sort()

    #print timeline_fnames
    for t in timeline_fnames:
        print t
        exponent = compute_exponent(t)
        hurst = compute_hurst(t)
        fractal_dim = compute_fractal_dim(t)
        print t, 0.5*exponent, hurst, fractal_dim
        #print partial info content
        #print info content
        #print power spectrum


def compute_exponent(tf):
 
  fit = {}
  coeff1 = {} 
  coeff2 = {}
  coeff3 = {}
  coeff4 = {}


  (fit[tf], coeff1[tf], coeff2[tf], coeff3[tf], coeff4[tf]) = load_timeline_info_pl(tf)
  distance_list = np.array([])
  fitness_list = np.array([])

  for i in range(1,len(fit[tf])):
      for j in range(1, len(fit[tf])):
          distance = log(math.fabs(metric(coeff1[tf][i], coeff2[tf][i], coeff3[tf][i], coeff4[tf][i], coeff1[tf][i], coeff2[tf][j], coeff3[tf][j], coeff4[tf][j])))
          fitness = log(math.fabs(fit[tf][i]-fit[tf][j]))
          distance_list = np.append(distance_list, distance)
          fitness_list = np.append(fitness_list, fitness)
      
   
  function = np.array([1.2])
  
  plsq_power = leastsq(residuals, function, args=(distance_list, fitness_list))

  return plsq_power[0]
  #poly_result_power = peval(fitness_list, plsq_power[0])


def residuals(p, y, x):
  ## a,b,c,d = p
  ## erra = y - (a + b*x + c*x*x + d*x*x*x)
  poly_result = 0
  for i in range(len(p)):
    poly_result += p[i] * x
  err = y - poly_result
  return err

def metric(coeff0i, coeff1i, coeff2i, coeff3i, coeff0i2, coeff1i2, coeff2i2, coeff3i2):
    result = sqrt(pow((coeff0i-coeff0i2),2.0)+pow((coeff1i-coeff1i2)/70,2.0)+pow((coeff2i-coeff2i2)/pow(70,2.0),2.0)+pow((coeff3i-coeff3i2)/pow(70,3.0),2.0))   
    return result
  
def load_timeline_info_pl(tf):
    
    lines = open(tf)
    fitness = []
    coeff1 = []
    coeff2 = []
    coeff3 = []
    coeff4 = []
    for line in lines:
        if line[0] == '#':
            continue
        words = string.split(line)
        fitness.append(float(words[1]))
        coeff1.append(float(words[7]))
        coeff2.append(float(words[8]))
        coeff3.append(float(words[9]))
        coeff4.append(float(words[10]))
        
    return (fitness, coeff1, coeff2, coeff3, coeff4)

def load_timeline_info_poly(tf):
    lines = open(tf)
    fitness = []

    for line in lines:
        if line[0] == '#':
            continue
        words = string.split(line)
        fitness.append(float(words[1]))
        x.append(float(words[2]))
  
        
    return (fitness, x)

def compute_hurst(tf):
    
    fit = {}
    x = {} 

    (fit[tf], x[tf]) = load_timeline_info_poly(tf)

    mean = 0
    sum = 0
    for i in range(1,len(fit[tf])):
        sum+=fit[tf][i]

    mean = sum/len(fit[tf])
    
    mean_adjusted = {}
    for j in len(fit[tf]):
        mean_adjusted[tf][j] = fit[tf][j] - mean

    cum_dev = {}
    for t in range(1,len(fit[tf])):
        sum_t = 0
        for i in range(1,t):
            sum_t+=mean_adjusted[tf][i]
        cum_dev[tf][t] = sum_t

        
    range = {}
    range_k = {}
    for k in range(1,len(fit[tf])):
        for i in range(1,k):
            range_k[tf][i] = cum_dev[i]
        range[tf][k] = max(range_k[tf]) - min(range_k[tf])


    sd = {}
    for m in range(1,len(fit[tf])):
        sum_m = 0
        for i in range(1,m):
            sum_m+=pow(fit[tf][i]-mean,2)
        sd[tf][m] = sqrt(sum_m/m)

    
    range_sd = {}
    for l in range(1, len(fit[tf])):
        range_sd[tf][l] = range[tf][l]/sd[tf][l]

    distance_list = np.array([])
    fitness_list = np.array([])

    for i in range(1,len(fit[tf])):
        distance = log(i)
        fitness = log(math.fabs(range_sd[tf][i]))
        distance_list = np.append(distance_list, distance)
        fitness_list = np.append(fitness_list, fitness)
      
   
    function = np.array([1.2])
  
    plsq_power = leastsq(residuals, function, args=(distance_list, fitness_list))

    return plsq_power[0]



    

def compute_fractal_dim(tf):
    
    fit = {}
    x = {} 

    (fit[tf], x[tf]) = load_timeline_info_poly(tf)

    L_m_k = {}
    sum_m = {}
    mean_m = {}
    
    for m in range(len(fit[tf])):
        for k in range(len(fit[tf])):
            sum =0
            for i in range((int)(len(fit[tf]) - m)/k):
                sum+=fit[tf][m+i*k] - fit[tf][m+i*k -k]
        L_m_k[tf][k][m] = (sum/k)*(len(fit[tf])-1)/((len(fit[tf])-m)/k)
        for j in range(1,m):
            sum_m[k]+=L_m_k[tf][k][j]

        for l in range(1,m):          
            mean_m[k] = sum_m[k]/l
           

    distance_list = np.array([])
    fitness_list = np.array([])

    for i in range(1,len(fit[tf])):
        distance = log(i)
        fitness = log(math.fabs(mean_m[tf][i]))
        distance_list = np.append(distance_list, distance)
        fitness_list = np.append(fitness_list, fitness)
      
   
    function = np.array([1.2])
    
    plsq_power = leastsq(residuals, function, args=(distance_list, fitness_list))

    return plsq_power[0] 
        
            



  
if __name__ == '__main__':
  __MAIN__()
