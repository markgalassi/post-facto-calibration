#! /bin/sh

GNUPLOT="gnuplot"
if [ -f /usr/lanl/bin/gnuplot ] ; then
  GNUPLOT="/usr/lanl/bin/gnuplot"
fi
echo GNUPLOT is $GNUPLOT

for f in fit_landscape_plane*
do
    ./fitness_landscapes.sh $f
done
pdfjoin plane_?_?-src*.pdf --outfile landscapes_combined.pdf
