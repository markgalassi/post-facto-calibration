#include "scurve-types.h"
#include "prototypes.h"

double step_size;

double shannon_entropy(AlgorithmType t, Poly scurve, double *y_readout){

  int x_yes = 0;
  int x_no = 0;
  double prob, se;
  double fitness = calc_fitness(scurve, y_readout);
  int n_entropy_tries = 1000, i=0;
  Poly scurve_current = scurve;

  /* based on which algo, take step */
  while(i<n_entropy_tries){
    switch(t) {
    case hill:
      scurve_current = hill_take_step(scurve);
      break;
    case adaptive:
      scurve_current = adapt_take_step(scurve, i, y_readout);
      break;
    case iterated:
      scurve_current = perturb_iterated(scurve);
      scurve_current = hill_take_step(scurve_current);
      break;
    case variable:
      scurve_current = variable_take_step(scurve, y_readout);
      break;
    default:
      break;
    }
    if(calc_fitness(scurve_current, y_readout) > fitness){
      x_yes++;
    }
    else{
      x_no++;
    }
    i++;
  }
  
  prob = (double) (1.0*x_yes)/(1.0*n_entropy_tries);
  if(x_yes == 0)
    {
    se = 0.0;}
  else
    {
      se = -prob*log(prob);
    }

  return se;
}


/* this is an adaptation of the adaptive random search algo. It will
   not update the step, because we are not looking to increase
   fitness, we are sampling according to the algo*/


Poly adapt_take_step(Poly scurve, int step, double *y_readout){
  Poly scurve_current;
  Poly pnormal, plarge;
  double step_size_large;

  adapt_prep_step(scurve, &pnormal, &plarge, step, step_size, &step_size_large);
  double fitness_pnormal = calc_fitness(pnormal, y_readout);
  double fitness_plarge = calc_fitness(plarge, y_readout);
  if(fitness_plarge > fitness_pnormal){
    scurve_current = plarge;
  }
  else{
    scurve_current = pnormal;
  }

  return scurve_current;


}



  
