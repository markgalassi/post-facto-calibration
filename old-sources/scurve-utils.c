#include "prototypes.h"
#include "constants.h"
#include "scurve-types.h"


/** 
 * 
 * Prepares a one dimensional y_real based on the mask, angle and
 * focal length
 * @return new malloc()ed y_real array
 */

int mask[] = {1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 
	      1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 
	      0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 
	      0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0};

/* int get_n_mask_bins() */
/* { */
/*   int n = sizeof(mask)/sizeof(mask[0]); */
/*   return n; */
/* } */

double y_readout_siman[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3715, 3678, 3617, 7490, 3799, 0, 0, 0, 0, 0, 3661, 3786, 3693, 7498, 3679, 3720, 3822, 3749, 3692, 7381, 3654, 3862, 3767, 3819, 3717, 0, 0, 0, 0, 3747, 3745, 3767, 3799, 3769, 3790, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 3765, 3768, 3820, 3828, 3678, 3864, 0, 0, 0, 0, 0, 0, 3782, 3802, 3656, 3671, 3764, 0, 0, 0, 0, 0, 0, 3785, 3811, 3837, 3699, 3798, 3788, 3771, 0, 3696, 3743, 3827, 3801, 3699, 0,0,0,0,0,0,0,0,0,0,0,0, 3754, 3753, 0, 3843, 3719, 3754, 3722, 0, 0, 0, 0, 0, 0, 3805, 3844, 3708, 3671, 0, 3634, 3745, 0, 0, 0, 0, 0, 0, 0, 3854, 3733, 0, 3720, 3803, 3744, 3773, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 3772, 0, 3750, 3773, 3759, 3767, 0, 0, 0, 0, 0, 0, 0, 0, 3662, 3724, 0, 3713, 3690, 3783, 3666, 0,0,0,0,0,0,0, 0,0,0,0,0,0,0, 3801, 3776, 0, 3822, 3738, 3777, 3829, 0, 0, 0, 0, 0, 0, 0, 0, 3723, 3834, 0, 3775, 3729, 3735, 3715, 3736, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3715, 3678, 3617, 7490, 3799, 0, 0, 0, 0, 0, 3661, 3786, 3693, 7498, 3679, 3720, 3822, 3749, 3692, 7381, 3654, 3862, 3767, 3819, 3717, 0, 0, 0, 0, 3747, 3745, 3767, 3799, 3769, 3790, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 3765, 3768, 3820, 3828, 3678, 3864, 0, 0, 0, 0, 0, 0, 3782, 3802, 3656, 3671, 3764, 0, 0, 0, 0, 0, 0, 3785, 3811, 3837, 3699, 3798, 3788, 3771, 0, 3696, 3743, 3827, 3801, 3699, 0,0,0,0,0,0,0,0,0,0,0,0, 3754, 3753, 0, 3843, 3719, 3754, 3722, 0, 0, 0, 0, 0, 0, 3805, 3844, 3708, 3671, 0, 3634, 3745, 0, 0, 0, 0, 0, 0, 0, 3854, 3733, 0, 3720, 3803, 3744, 3773};


void scurve_check_setup()
{
  int n = sizeof(mask)/sizeof(mask[0]);
  assert(n == n_mask_bins);
  n = sizeof(y_readout_siman)/sizeof(y_readout_siman[0]);
  assert(n == n_det_bins);
  assert(source.z != 0);
  /*assert(source.z >= focal_length);*/
}

double *generate_y_real()
{
  double *y_real = malloc(n_det_bins*sizeof(*y_real));
  int i;
  int n_photons = n_photons_default; /* we shoot photons through the mask */
  memset(y_real, 0, n_det_bins*sizeof(*y_real));
  if(is_finite(source))
    {
      for(i=0;i<n_photons;i++)
	{
	  double photon_angle = rand_in_bounds(-M_PI/2.0,M_PI/2.0);
	  if(det_ind_legal_finite_distance(photon_angle)){
	    int det_ind = det_ind_finite_distance(photon_angle);
	    if((det_ind >= 0) && (det_ind < n_det_bins))
	      { 
		++y_real[det_ind];
	      }
	  }
	}
    }

  else
      {
	for(i=0;i<n_photons;i++)
	  {
	    int det_ind = random() % n_det_bins;
	    if (det_ind_is_legal(det_ind)) 
	      {
		++y_real[det_ind];
	      }
	  }
    /* to generate y_real we pick a random detector pixel and then see
       if it's compatible with the mask pattern and the angle */

      }
  return y_real;
}

int det_ind_is_legal(int det_ind){

  double landing_location_mask;
  /* computing the landing location of a beam shot at the angle shooting_angle 
     on the mask */
  if(source.a < 0)
    {      
      landing_location_mask =((det_ind)*detector_height)/n_det_bins + 
	focal_length*tan(source.a);
    }
  else 
    {
      landing_location_mask =((det_ind)*detector_height)/n_det_bins - 
	focal_length*tan(source.a);
    }
  /* now computing the pixel corresponding to that location and checking if 
     slit is open */
  int landing_location_mask_pixel = round((landing_location_mask*n_mask_bins/
					      detector_height));
  if((landing_location_mask_pixel >= 0) && (landing_location_mask_pixel < 
					    n_mask_bins) 
     && (mask[landing_location_mask_pixel] == 1))
    {
      return 1;
    }
  else
    return 0;
}

int det_ind_legal_finite_distance(double photon_angle)
{
  /* computing the location of the beam on the mask */
  double hit_location_mask = source.y + tan(photon_angle)*
    (source.z - focal_length);
  /* now computing the corresponding pixel and checking that the slit is open */
  int hit_location_mask_pixel = round(hit_location_mask*
				      n_mask_bins/detector_height);
  if((hit_location_mask_pixel >=0) && 
     (hit_location_mask_pixel < n_mask_bins) && 
     (mask[hit_location_mask_pixel] == 1))
    return 1;
  else
    return 0;
}

int det_ind_finite_distance(double photon_angle)
{
  int det_ind;
  /* computing location of beam on mask */
  double hit_location_mask = source.y + tan(photon_angle)*
    (source.z - focal_length);
  /* now computing corresponding location on detector */
  double hit_location_detector = hit_location_mask + tan(photon_angle)*
    focal_length;
  /* and corresponding pixel on detector */
  det_ind =  round(hit_location_detector * n_det_bins/detector_height);

  return det_ind;  
}

double *y_real2y_readout(double *y_real, Poly poly)
{
  double *y_readout = malloc(n_det_bins*sizeof(*y_readout));
  apply_distortion(y_real, y_readout);
  return y_readout;
}

/** 
 * Reads a one-dimensional coded y_readout from a file.
 * 
 * @param y_readout_fname data file with y_readout information in it
 * @param y_readout_p we allocate the y_readout array and return it here
 * @param n_pixels in this variable we return the number of y_readout pixels
 */
void read_y_readout(char *y_readout_fname, double **y_readout_p)
{
  FILE *fp = fopen(y_readout_fname, "r");
  assert(fp);
  int *n_pixels = 0;
  int y_readout_val;
  char line[MAX_LINE_LEN];
  int i = 0;
  while (fgets(line, MAX_LINE_LEN, fp) != NULL) {
    if (line[0] == '#') {       /* comment */
      char *pos;
      if ((pos = strstr(line, "n_det_bins")) != NULL) {
        /* read in the position */
        if (sscanf(pos + strlen("n_det_bins"), "%d", n_pixels) != 1) {
          /* make sure we handle bad formats */
          fprintf(stderr, "incorrect data in y_readout file %s\n", 
		  y_readout_fname);
          exit(1);
        }
      }
      continue;       
    }
    /* if it's not a comment we read in a y_readout element */
    if (sscanf(line, "%d", &y_readout_val) != 1) {
      fprintf(stderr, "incorrect data in y_readout file %s\n", 
	      y_readout_fname);
      exit(1);
    }
    i += 1;
  }

  fclose(fp);
}

void prepare_mask_and_readout(double **readout_p, DataOrigin y_readout_origin)
{

  if (y_readout_origin == generate) {
    double *y_real = generate_y_real();
    *readout_p = y_real2y_readout(y_real, scurve_true_make());
    /* NOISE */
    /* add_noise(noise_default, readout_p); */

    free(y_real);
  }
  if (y_readout_origin == from_file) {
    read_y_readout("y_readout_sample.dat", readout_p);
  }
  assert(*readout_p);
}


int open_mask(double y)
{
  /* checking if the slit corresponding to the stretched pixel for the 
     finite distance case is open */
  /* this y is on the mask */
  int mask_pixel;
  if(is_finite(source))
    {
      mask_pixel = floor(y*n_mask_bins/detector_height);
      mask_pixel = floor(mask_pixel*(source.z+focal_length)/source.z);
    }
  else 
    {
      mask_pixel = floor(y*n_mask_bins/detector_height);
    }
  assert(mask_pixel >= 0 && mask_pixel < n_mask_bins);
  if((mask_pixel < n_mask_bins) && (mask_pixel >=0) && (mask[mask_pixel]))
    {
      return 1;
    }
  else
    return 0;
  
}

/* void print_state_header(char *fname, char *info, Setup initial) */
void print_state_header(char *fname)
{
  FILE *fp = fopen(fname, "w");
  assert(fp);
  /* fprintf(fp, "%s", info);  */    /* metadata of various sorts */
  fprintf(fp, "# iteration fitness -- scurve_coefficients\n");
  fclose(fp);
}


/** 
 * prints out the state of our search
 * 
 * @param step 
 * @param fitness 
 * @param poly 
 */
void print_state(char *fname, int step, double fitness, Poly poly, 
		 double step_size, double entropy, double metric)
{
  int i;
  FILE *fp = fopen(fname, "a");
  assert(fp);
  fprintf(fp, "%3d %9g %7g %7g %7g --", step, fitness, step_size, entropy, metric);
  fprintf(stdout, "%3d %9g %7g %7g %7g --", step, fitness, step_size, entropy, metric);
  for (i = 0; i < poly.n; ++i) {
    fprintf(fp, "  %5g", poly.c[i]);
    fprintf(stdout, "  %5g", poly.c[i]);
  }
  fprintf(fp, "\n");
  fprintf(stdout, "\n");
  fclose(fp);
}

/** 
 * prints diagnostics of this fitness evaluation into the specified
 * directory
 * 
 * @param outdir where to put the output files
 * @param scurve 
 * @param y_real_guess 
 * @param convolution 
 */
void print_fitness_diagnostics(char *outdir, int step, Poly scurve,
                               double *y_readout, AlgorithmType t, 
			       double step_size)
{
  int i;
  char fname[MAX_FILENAME_LEN];
  FILE *fp;

  double *convolution = malloc(n_det_bins*sizeof(*convolution));
  double *y_real_guess = malloc(n_det_bins*sizeof(*y_real_guess));
  scurve_poly(y_readout, y_real_guess, scurve);
  char run_id_str_eps[MAX_FILENAME_LEN];
  fill_convolution(y_real_guess, convolution);

  if (is_finite(source)) {
    sprintf(run_id_str_eps, "eps%1.5g_srcY%1.5g_Z%1.5g",  
	    step_size, source.y, source.z);
    
  } else {
    sprintf(run_id_str_eps, "eps%1.5g_srcA%1.5g",
	    step_size, source.a);
    
  }
    
  if(t == hill) {
    sprintf(fname, "%s/%s_%s_%s_%d_%s", outdir, "hill", run_id_str_eps, 
	    "step", step, "all");
  }
  else 
    if(t == adaptive){
      sprintf(fname, "%s/%s_%s_%s_%d_%s", outdir, "adaptive", 
	      run_id_str_eps, "step", step, "all");
    }
  
    else 
      if(t == iterated){
	sprintf(fname, "%s/%s_%s_%s_%d_%s", outdir, "iterated", 
		run_id_str_eps, "step", step, "all");}
      else
	if(t == variable){
	  sprintf(fname, "%s/%s_%s_%s_%d_%s", outdir, "variable", 
		  run_id_str_eps, "step", step, "all");}
 
  fp = fopen(fname, "w");
  assert(fp);
  fprintf(fp, "# step: %d\n", step);
  fprintf(fp, "# n_det_bins: %d\n", n_det_bins);
  fprintf(fp, "# det_bin, scurve, y_real_guess, convolution\n");
  for (i = 0; i < n_det_bins; ++i) {
    double location = (i * detector_height) / n_det_bins;
    double scurve_mapped_location = poly_map_point(scurve, location);
    fprintf(fp, "%5d  %8g    %8g      %8g\n",
            i, scurve_mapped_location, y_real_guess[i], convolution[i]);
  }
  fclose(fp);
  free(convolution);
  free(y_real_guess);
}

/** 
 * 
 * @param argc 
 * @param argv 
 */
void parse_options(int argc, char *argv[])
{
  int opt;
  extern char *optarg;
  extern int optind, opterr, optopt;
  char * comma_pos;
  char * comma_pos_g_1; 
  char * comma_pos_g_2;

  while ((opt = getopt(argc, argv, "e:n:s:o:")) != -1) {
    switch (opt) {
    case 'e':
      step_size = atof(optarg);
      assert(step_size > 0);
      break;
    case 'n':
      n_steps = atoi(optarg);
      assert(n_steps > 0);
      break;
      /*
    case 'h':
      entropy_indicator = atoi(optarg);
      break;
      */
    case 's':
      comma_pos = strchr(optarg, ',');
      if(comma_pos == NULL){
	source.a = atof(optarg);
	source.z = -1.0;
	if(comma_pos < 0)
	  fprintf(stderr, "Usage: %s [-e step_size] [-n n_steps] [-s double,double] [-m metric] [-h entropy_indicator] \n", argv[0]);	

      }
      else {
	sscanf(optarg, "%lf,%lf", &source.y, &source.z);
      }
      break;
    case 'o':
      comma_pos_g_1 = strchr(optarg, ',');
      comma_pos_g_2 = strrchr(optarg, ',');     
      if(comma_pos_g_1 == comma_pos_g_2){
	sscanf(optarg, "%lf,%lf", &source.a, &step_size);
	source.z = -1.0;
	if(comma_pos_g_1 < 0)
	  fprintf(stderr, "Usage: %s [-e step_size] [-n n_steps] [-s double,double]  \n", argv[0]);	
      }
      else {
	sscanf(optarg, "%lf,%lf,%lf", &source.y, &source.z,&step_size);
      }
      break;
    default: /* '?' */
      fprintf(stderr, "Usage: %s [-e step_size] [-n n_steps] [-s double,double] [-m metric] [-o noise_percentage] [-h entropy_indicator]  \n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

}

void setting_params(int argc, char *argv[], AlgorithmType t){

  /*DataOrigin y_readout_origin = generate;*/

  /* first set everything to defaults */ 
  /* global variables */
  n_steps = n_steps_default; 
  /* metric = metric_default; */ /* we use the normal metric, ratio */
  source.a = a_default;
  source.y = y_default;
  source.z = z_default;
  noise_percentage = noise_percentage_default;
  step_size = step_size_default;
    
  /* then parse options */
  parse_options(argc, argv);
  srandom(123456);

  if(is_finite(source)){
    real_shift = floor(((source.y/detector_height)*n_det_bins));}
  else{
    real_shift = floor(((tan(source.a)*focal_length)*n_det_bins/detector_height));}
    
  /*scurve_initial = make_poly(0.0, 0.05 +1+2*distortion_c3,

  /*
  scurve_initial = make_poly(0.0, 0.05 +1+2*distortion_c3,

				  0.05 + distortion_c3);*/

  scurve_initial = make_poly(1.2, -0.9, 0.8, 1.3);
  /* start from more random scurves  */
  /*scurve_initial = make_poly(0.9, -2.3, 0.1, 0.8); */

  scurve_true = scurve_true_make();  
}

int countChars( char* s, char c )
{
    return *s == '\0'
              ? 0
              : countChars( s + 1, c ) + (*s == c);
}

void make_directories(char *output_dir){
  
  char run_id_str[MAX_FILENAME_LEN];
  if (is_finite(source)) {
    sprintf(run_id_str, "srcY%1.5g_Z%1.5g", source.y, source.z);	
  } else {
    sprintf(run_id_str, "srcA%1.5g", source.a);
  }

  sprintf(output_dir, "outdir_%s",run_id_str);
  mkdir(output_dir, 0755);  

  /* write out the "true" s-curve */
  char scurve_true_fname[MAX_FILENAME_LEN];
  sprintf(scurve_true_fname, "%s/scurve_true", output_dir);
  scurve_write(scurve_true_make(),
               scurve_true_fname, "the true scurve");
  /* write out the mask we use */
  char mask_fname[MAX_FILENAME_LEN];
  sprintf(mask_fname, "%s/mask", output_dir);
  FILE *fp = fopen(mask_fname, "w");
  assert(fp);
  int i;
  for (i = 0; i < n_mask_bins; ++i) {
    fprintf(fp, "%d\n", mask[i]);
  }
  fclose(fp);
}

/** 
 * simple writing of an s-curve to a file; used at the start to save
 * off the true s-curve.
 * 
 * @param scurve 
 * @param filepath 
 */
void scurve_write(Poly scurve, char *filepath, char *comment)
{
  FILE *fp = fopen(filepath, "w");
  assert(fp);
  fprintf(fp, "# FILE_TYPE: scurve\n");
  fprintf(fp, "# COMMENT: %s\n", comment);
  assert(scurve.n == 4);        /* for now we assume 3rd degree polynomials */
  fprintf(fp, "# n_coefficients: %d\n", scurve.n);
  int i;
  for (i = 0; i < scurve.n; ++i) {
    fprintf(fp, "%12.6g\n", scurve.c[i]);
  }
  fclose(fp);
}

void print_metadata(char *filepath){

  FILE *fp = fopen(filepath, "w");

  fprintf(fp, "# y_source %g z_source %g shooting_angle %g \n", 
	  source.y, source.z, source.a);  
  fprintf(fp, "# focal_length %g n_det_bins %d n_mask_bins %d" 
	  "detector_height %g step_size %g real_shift %d\n", 
	  focal_length, n_det_bins, n_mask_bins, detector_height, step_size, 
	  real_shift);
  fprintf(fp, "# noise_percentage %g \n", 
	  noise_percentage);  

  char date_str[100];
  struct tm *local;
  time_t t;
  time(&t);
  local = localtime(&t);
  strftime(date_str, 99, "## DATE_INVOCATION: %FT%T %z\n", local);
  fprintf(fp, date_str);
  /*
  char CFLAGS[100];
  CFLAGS = "`hg heads | grep ^changeset: | awk '{print $2}'`";
  char *mercurial_revsion = "$hg id -i$";
  fprintf(fp, CFLAGS); */
  fclose(fp);

}
int is_finite(Source_type source){

  if(source.z < 0) 
    return 0;
  else
    return 1;
}


double metric_coeffs(Poly scurve1, Poly scurve2){
  int i;
  double distance = 0.0;
  for(i=0; i<scurve1.n; i++){
    distance+= pow((scurve1.c[i] -scurve2.c[i]),2.0)/pow(70.0,i);
  }

  distance = sqrt(distance);
  return distance;
  

}


void assign_hyperplane(double *coeff_1, double *coeff_2, int i2, int j2){

  switch(i2){
  case 0:
    switch(j2){
    case 1:
      *coeff_1 = -3.0*distortion_c3;
      *coeff_2 = distortion_c3;
      break;
    case 2:
      *coeff_1 = 1.0+2.0*distortion_c3;
      *coeff_2 = distortion_c3;
      break;
    case 3: 
      *coeff_1 = 1.0+2.0*distortion_c3;
      *coeff_2 = -3.0*distortion_c3;
      break; 
    default:
      break;
    }
    break;
  case 1:
    switch(j2){
    case 2:
      *coeff_1 = 0;
      *coeff_2 = distortion_c3;
      break;
    case 3:
      *coeff_1 = 0;
      *coeff_2 = -3.0*distortion_c3;
      break;
    default:
      break;
    }
    break;
  case 2:
    switch(j2){
    case 3:
      *coeff_1 = 0;
      *coeff_2 = 1.0+2.0*distortion_c3;
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }

}
