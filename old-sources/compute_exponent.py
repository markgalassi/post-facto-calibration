#! /usr/bin/env python

"""
compute power law exponents
"""

import sys
import glob
import os
import re
from pprint import pprint
import string
import getopt

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np
import math
from matplotlib.mlab import griddata

from math import log
from math import sqrt 
import numpy as np
import scipy
from scipy.optimize import leastsq

colormap = cm.jet

def __MAIN__():
    outdir = sys.argv[1]

    timeline_fnames = glob.glob(outdir + '/*timeline_incr')
    timeline_fnames.sort()

    #print timeline_fnames
    for t in timeline_fnames:
        #print t
        exponent = compute(t)
        print t, 0.5*exponent


def compute(tf):
 
  fit = {}
  coeff1 = {} 
  coeff2 = {}
  coeff3 = {}
  coeff4 = {}


  (fit[tf], coeff1[tf], coeff2[tf], coeff3[tf], coeff4[tf]) = load_timeline_info_pl(tf)
  distance_list = np.array([])
  fitness_list = np.array([])

  for i in range(1,len(fit[tf])):
      distance = log(math.fabs(metric(coeff1[tf][i], coeff2[tf][i], coeff3[tf][i], coeff4[tf][i], coeff1[tf][i], coeff2[tf][i-1], coeff3[tf][i-1], coeff4[tf][i-1])))
      fitness = log(math.fabs(fit[tf][i]-fit[tf][i-1]))
      distance_list = np.append(distance_list, distance)
      fitness_list = np.append(fitness_list, fitness)
      
   
  function = np.array([1.2])
  
  plsq_power = leastsq(residuals, function, args=(distance_list, fitness_list))

  return plsq_power[0]
  #poly_result_power = peval(fitness_list, plsq_power[0])


def residuals(p, y, x):
  ## a,b,c,d = p
  ## erra = y - (a + b*x + c*x*x + d*x*x*x)
  poly_result = 0
  for i in range(len(p)):
    poly_result += p[i] * x
  err = y - poly_result
  return err

def metric(coeff0i, coeff1i, coeff2i, coeff3i, coeff0i2, coeff1i2, coeff2i2, coeff3i2):
    result = sqrt(pow((coeff0i-coeff0i2),2.0)+pow((coeff1i-coeff1i2)/70,2.0)+pow((coeff2i-coeff2i2)/pow(70,2.0),2.0)+pow((coeff3i-coeff3i2)/pow(70,3.0),2.0))   
    return result
  

def load_timeline_info_pl(tf):
    
    lines = open(tf)
    fitness = []
    coeff1 = []
    coeff2 = []
    coeff3 = []
    coeff4 = []
    for line in lines:
        if line[0] == '#':
            continue
        words = string.split(line)
        fitness.append(float(words[1]))
        coeff1.append(float(words[5]))
        coeff2.append(float(words[6]))
        coeff3.append(float(words[7]))
        coeff4.append(float(words[8]))
        
    return (fitness, coeff1, coeff2, coeff3, coeff4)

  
if __name__ == '__main__':
  __MAIN__()
