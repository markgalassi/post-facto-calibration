#!/bin/bash
# SCRIPT:  make_list_good_runs.sh
# PURPOSE: Process the list file of good runs and outputs the necessary files, and creates the plots

LIST_ALGOS_DO=".././scurve-hill .././scurve-adaptive_random .././scurve-iterated .././scurve-variable .././scurve-fit-landscapes"

FILENAME=LIST_GOOD_RUNS

IFS="
"

#make a special directory
#cmd_create_dir="mkdir 
# Loop through the file
for algo in $LIST_ALGOS_DO
do
    for line in `cat $FILENAME`; do
	echo $line
	cmd="$algo $line"
	eval $cmd
    done
done
cmd_plots_evol="./plot_run_evolution.py"
eval $cmd_plots_evol
cmd_plots_fit="./fit_land_traj.py"
eval $cmd_plots_fit
cmd_plots_info="./info_theory_measures.py"
eval $cmd_plots_info
cmd_move_plots="mv */*.pdf ../../notable-plots"
eval $cmd_move_plots
#echo -e "\nTotal $count Lines read"