#! /usr/bin/env python

"""
make plots of a run -- shows some visualization of all the data in
the given run output directory.  this works by calling matplotlib to
create plots, then it saves them as PDF files.
"""

import sys
import glob
import os
import re
from pprint import pprint
import string
import getopt

import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np

markers = ('^', 'o', 'p', 's', '8', 'h', 'x', 'v')
colormap = cm.jet
correlation_style = 'same'              # 'full' or 'same'
#ncolormap = cm.bwr
#colormap = cm.seismic

global_fit_max = sys.float_info.min # initial values before we find them
global_fit_min = sys.float_info.max

def __MAIN__():
  ## first check command line options; these are "-i" to do an
  ## interactive show at the end, and then a set of directory names
  ## (if they are given); if no directory names are given then we use
  ## all directories that match the glob 'outdir_*src*'
  interactive = False
  (opts, args) = getopt.getopt(sys.argv[1:], 'i', [])
  for o, a in opts:
    if o == '-i':                       # get a single date
      interactive = True
  if args:
    outdir_list = args
  else:
    outdir_list = glob.glob('outdir_*')
  outdir_list.sort()

  pprint('# processing this list of output directories:')
  pprint(outdir_list)
  ## now the actual work of generating the plots
  results_dict = {}
  for outdir in outdir_list:
    run_id_str = outdir[len('outdir_'):]
    scurve_true = scurve_load_coefficients(outdir, 'scurve_true')
    mask = mask_load(outdir, 'mask')
    timeline_fnames = glob.glob(outdir + '/*timeline_incr*')
    make_evolution_plots(timeline_fnames, run_id_str, scurve_true, mask, outdir)

  ## now a little feature: if the command line says "interactive" 
  if interactive:
    plt.show()

def process_run_data(outdir):
  """process the run data from an "outdir_..." directory; the
  directories look something like: outdir_hill-srcA0.2, or
  ./outdir_srcY0.8_Z23.5, and we can parse them
  to get the run parameters.  What we generate is a PDF file for
  each step recorded in the run.  We also load some long-range
  data."""
  run_id_str = outdir[len('outdir_'):]
  run_parts = re.split("[_-]", run_id_str)
  algo = run_parts[0]
  ## we probably don't need timeline data at this point
  scurve_true = scurve_load_coefficients(outdir, 'scurve_true')
  mask = mask_load(outdir, 'mask')
  output_files = glob.glob(outdir + '/*_all')
  output_files.sort()
  for fname in output_files:
    make_single_iter_plots(fname, scurve_true, run_id_str, mask)    
    plt.savefig(fname + '.pdf', format='pdf')
    plt.close()
    print 'saved %s.pdf' % fname
  ## when we're done we concatenate it all
  os.system('pdfjoin --quiet %s/*_all.pdf --outfile %s/%s_combined.pdf'
            % (outdir, outdir, run_id_str))

def make_evolution_plots(timeline_fnames, run_id_str, scurve_true, mask, f):

  for tf in timeline_fnames: 

    print tf
    # create arrays for each timeline 
    iters = {}
    fit_list = {}
    fit = {}
    step_size = {} 
    initial = {}
    final = {}
    fit_differences = {}
    max_fit_differences = {}
    highest_jump1 = {}
    before_jump1 = {}
    index_max_differences = {}
    evol_files_initial = {}
    evol_files_before_jump1 = {}
    evol_files_jump1 = {}
    evol_files_final = {}

    total_length = len(tf)
    length = len(f + '/')
    length_end = len("_fit_timeline_incr")
    size = total_length - length_end

    run_id_tf = tf[int(length-1):int(size)]
    print run_id_tf
    # load trajectory file
    (iters[tf], fit[tf], step_size[tf]) = load_timeline(tf)
    if len(iters[tf]) == 0:
      print 'file %s not found; continuing' % tf
      continue
 
    initial[tf] = iters[tf][0]
    final[tf] = iters[tf][-1]

    max_fit_differences[tf] = 0
    index_max_differences[tf] = 0
    
    # find the maximum jump in fitness and its corresponding index 
    for j in range(1,(len(fit[tf]))):
      if (fit[tf][j] - fit[tf][j-1]) > max_fit_differences[tf]:
        max_fit_differences[tf] = (fit[tf][j] - fit[tf][j-1])
        index_max_differences[tf] = j
        
    highest_jump1[tf] = iters[tf][index_max_differences[tf]]
    before_jump1[tf] = iters[tf][index_max_differences[tf]-1]
    

    # decide which files I want to look at, based on what I found above
    evol_files_initial[tf] = f + '/' + run_id_tf + '_step_' + str(initial[tf]) + '_all'
    evol_files_before_jump1[tf] = f + '/' + run_id_tf + '_step_' + str(before_jump1[tf]) + '_all'
    evol_files_jump1[tf] = f + '/' + run_id_tf + '_step_' + str(highest_jump1[tf]) + '_all'
    evol_files_final[tf] = f + '/' + run_id_tf + '_step_' + str(final[tf]) + '_all' 

    #make the plots for those files 
    make_single_iter_plots(evol_files_initial[tf], scurve_true, run_id_str, mask)
    plt.savefig(f + '/' + run_id_tf + '_initial.pdf', format='pdf')
    plt.close()

    make_single_iter_plots(evol_files_before_jump1[tf], scurve_true, run_id_str, mask)
    plt.savefig(f + '/' + run_id_tf + '_before_jump1.pdf', format='pdf')
    plt.close()

    make_single_iter_plots(evol_files_jump1[tf], scurve_true, run_id_str, mask)
    plt.savefig(f + '/' + run_id_tf +  '_jump1.pdf', format='pdf')
    plt.close()  

    make_single_iter_plots(evol_files_final[tf], scurve_true, run_id_str, mask)
    plt.savefig(f + '/' + run_id_tf +  '_final.pdf', format='pdf')
    plt.close()  

    print 'done', f 

def make_single_iter_plots(fname, scurve_true, run_id_str, mask):
  lines = open(fname).readlines()
  det_bins = np.array([])
  scurve = np.array([])
  y_real_guess = np.array([])
  conv = np.array([])
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    #print words
    det_bins = np.append(det_bins, int(words[0]))
    scurve = np.append(scurve, float(words[1]))
    y_real_guess = np.append(y_real_guess, int(words[2]))
    conv = np.append(conv, float(words[3]))
  fig = plt.figure()
  #fig.set_xlabel('detector pixel')
  ax1 = fig.add_subplot(4, 1, 1)
  ax1.set_ylabel('mask')
  plt.bar(np.arange(len(mask)), mask, label='mask')
  prepare_legend(plt, loc='lower center')

  ax2 = fig.add_subplot(4, 1, 2)
  ax2.set_ylabel('y_real_guess')
  plt.bar(det_bins, y_real_guess, label='y_real_guess')
  prepare_legend(plt, loc='lower center')

  ax3 = fig.add_subplot(4, 1, 3)
  ax3.set_ylabel('cross-correlation')
  plt.bar(det_bins, conv, label='cross-correlation')
  prepare_legend(plt, loc='lower center')

  ax4 = fig.add_subplot(4, 1, 4)
  ax4.set_ylabel('scurve(y)')
  plt.plot(scurve, label='this iteration')
  L = 2.0                     # FIXME: must get this from the metadata
  true_scurve_values = scurve_apply(scurve_true, det_bins, L)
  plt.plot(true_scurve_values, label='true s-curve')
  diff = scurve - true_scurve_values
  plt.plot(diff, label='difference')
  prepare_legend(plt, loc='upper left')


def make_run_plots(results):
  pass

def plot_fitnesses(outdir_list):
  """prepare pyplot figures for fitness and for step size"""
  fig1 = plt.figure(1)
  ax1 = fig1.add_subplot(1,1,1)
  ax1.set_xlabel('iteration')
  ax1.set_ylabel('fitness')
  plt.title('fitness as a function of iteration')
  fig2 = plt.figure(2)
  plt.xlabel('iteration')
  plt.ylabel('step size')
  plt.title('step size as a function of iteration')
  fig3 = plt.figure(3)
  ax3 = plt.axes()
  ax3.set_yticks([])            # no x or y ticks on the overall plots
  ax3.set_xticks([])

  plt.title('step size and fitness correlations')
  iters = {}
  fit = {}
  fit_delta = {}
  fit_maxes = {}
  fit_minima = {}
  step_size = {}
  total_fit = np.array([])
  total_plateau_duration = np.array([])
  for f in outdir_list:
    ## cycle through all directories that have a fitness_timeline file
    ## SENSITIVE TO NAME CHANGE
    fname = glob.glob(f + '/' + '*fit_timeline')[0]
    (iters[f], fit_list, step_size[f]) = load_timeline(fname)
    if len(iters[f]) == 0:
      print 'file %s not found; continuing' % fname
      continue
    fit[f] = np.array(fit_list)
    fit_delta[f] = fit_list2fit_delta(fit[f])
    (fit_maxes[f], fit_minima[f]) = (np.max(fit[f]), np.min(fit[f]))
    total_fit = np.append(total_fit, fit_delta[f])
    delta_iters = fit_list2fit_delta(iters[f])
    #total_step_size = np.append(total_step_size, step_size[f])
    total_plateau_duration = np.append(total_plateau_duration, delta_iters)
  ## now that we have all data, find the global max/min fitness
  global global_fit_max
  global global_fit_min
  global_fit_max = np.max(fit_maxes.values())
  global_fit_min = np.min(fit_minima.values())

  n_plots = len(iters.keys())
  i = 0
  for f in outdir_list:
    if not fit.has_key(f):
      return
    fit_offset = fit[f] - global_fit_min
    plt.figure(1)
    plt.plot(iters[f], fit[f], label=f[len('outdir_'):])
    plt.scatter(iters[f], fit[f], c=fit_offset, s=50*fit_offset,
                alpha=0.9, cmap=colormap, marker=markers[i%len(markers)],
                vmin = 0, vmax = global_fit_max-global_fit_min)
    plt.figure(2)
    plt.plot(iters[f], step_size[f], label=f[len('outdir_'):])
    plt.scatter(iters[f], step_size[f], c=fit_offset, s=50*fit_offset,
                alpha=0.9, cmap=colormap, marker=markers[i%len(markers)],
                vmin = 0, vmax = global_fit_max-global_fit_min)
    fig3 = plt.figure(3)
    #xcorr_size_fit = np.correlate(step_size[f], fit_delta[f], correlation_style)
    xcorr_size_fit = np.correlate(iters_delta, fit_delta[f], correlation_style)
    #acorr_size = np.correlate(step_size[f], step_size[f], correlation_style)
    acorr_size = np.correlate(iters_delta, iters_delta, correlation_style)
    acorr_fit = np.correlate(fit_delta[f], fit_delta[f], correlation_style)
    print f, len(step_size[f])
    for (hist, plot_no, name) in ((xcorr_size_fit, 1, 'xcorr size fit'),
                                  (acorr_fit, 3, 'acorr fit'),
                                  (acorr_size, 2, 'acorr size')):
      ax3 = fig3.add_subplot(n_plots, 3, 3*i+plot_no)
      plt.xticks(fontsize=5)
      plt.yticks(fontsize=4)
      plt.bar(range(len(hist)), hist,
              label=f[len('outdir_'):] + ' ' + name)
      leg = ax3.legend(loc='lower center', shadow=True, ncol=1, prop={'size':4})
      leg.get_frame().set_alpha(0.7)

    i += 1

  plt.figure(1)                         # switch to fitness
  #cbar = plt.colorbar(ax1.imshow(), ticks = [0, global_fit_max-global_fit_min])
  cbar = plt.colorbar(ticks = [0, global_fit_max-global_fit_min])
  cbar.ax.set_yticklabels(['Low', 'High'])
  cbar.set_label(r'fitness')
  plt.xscale('log')
  prepare_legend(plt, loc='lower right')
  plt.savefig('fitness_sweep.pdf', format='pdf')
  plt.close()
  print 'saved fitness_sweep.pdf'
  plt.figure(2)                         # switch to step size
  cbar = plt.colorbar(ticks = [0, global_fit_max-global_fit_min])
  cbar.ax.set_yticklabels(['Low', 'High'])
  cbar.set_label(r'fitness')
  plt.xscale('log')
  plt.yscale('log')
  prepare_legend(plt)
  plt.savefig('step_size_sweep.pdf', format='pdf')
  plt.close()
  print 'saved step_size_sweep.pdf'
  plt.figure(3)                         # switch to step distributions
  plt.savefig('step_acorr.pdf', format='pdf')
  plt.close()
  print 'saved step_acorr.pdf'
  ## now add a single plot for a cross correlation of total step size
  ## and fitness pictures
  fig4 = plt.figure(4)
  ## first get the list of sort indices from the step size list, so we
  ## can resort both lists in sync
  sort_indices_fit = total_fit.argsort()
  sort_indices_plateau = total_plateau_duration.argsort()
  total_fit_plateau_corr = np.correlate(total_fit[sort_indices_fit],
                                        total_plateau_duration[sort_indices_fit], 
                                        correlation_style)
  total_fit_acorr = np.correlate(total_fit[sort_indices_plateau],
                                 total_fit[sort_indices_plateau], correlation_style)
  total_plateau_duration_acorr = np.correlate(total_plateau_duration
                                              [sort_indices_plateau],
                                              total_plateau_duration
                                              [sort_indices_plateau], correlation_style)
  ## print total_fit_corr
  ax4 = fig4.add_subplot(5, 2, 1)
  plt.yscale('log')
  plt.bar(range(len(total_fit)), total_fit[sort_indices_plateau],
          label='log of fitness sorted by plateau duration')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 2, 2)
  plt.yscale('log')
  plt.hist(total_fit[sort_indices_plateau], 100, 
           label='log hist of combined fittness values')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 2, 3)
  plt.yscale('log')
  plt.bar(range(len(total_fit)), total_plateau_duration[sort_indices_plateau],
          label='log of plateau durations')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 2, 4)
  plt.yscale('log')
  plt.hist(total_plateau_duration[sort_indices_plateau], 100,
           label='log hist of plateau durations')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 1, 3)
  plt.bar(range(len(total_fit_plateau_corr)), total_fit_plateau_corr,
          label='corr of combined fitness and plateau duration')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 1, 4)
  plt.bar(range(len(total_fit_acorr)), total_fit_acorr,
          label='autocorrelation of combined fitness array')
  prepare_legend(plt, loc='lower center')
  ax4 = fig4.add_subplot(5, 1, 5)
  plt.bar(range(len(total_plateau_duration_acorr)), total_plateau_duration_acorr,
          label='autocorrelation of combined array of plateau durations')
  prepare_legend(plt, loc='lower center')
  plt.savefig('total_correlation.pdf', format='pdf')
  plt.close()
  print 'saved total_correlation.pdf'

def plot_trajectories(outdir_list):
  """runs the plotting commands for trajectories.  this makes 3D plots
  that show the steps taken.  they are projected down into 4 3D plots,
  since the search space is 4 dimensional."""
  global global_fit_max
  global global_fit_min
  for triplet in ((5,6,7), (4, 6, 7), (4, 5, 7), (4, 5, 6)):
    fig = plt.figure()
    #plt.subplots_adjust(bottom=0.1)
    ax = Axes3D(fig)
    i = 0
    for fitness_dir in outdir_list:
      fitness_file = glob.glob(fitness_dir + '/' + '*fit_timeline')[0]
      (x, y, z, fit_list) = load_trajectory(fitness_file, triplet)
      fit = np.array(fit_list)
      fit_offset = fit - global_fit_min
      ax.plot(x, y, z, label=fitness_dir[len('outdir_'):])
      p = ax.scatter(x, y, z, c=1000*fit_offset, s=30*fit_offset,
                     alpha=0.9, cmap=colormap,
                     vmin = 0, vmax = global_fit_max-global_fit_min,
                     marker=markers[i%len(markers)])
      i += 1
    ax.set_xlabel('coefficient $c_{%d}$' % (triplet[0]-4))
    ax.set_ylabel('coefficient $c_{%d}$' % (triplet[1]-4))
    ax.set_zlabel('coefficient $c_{%d}$' % (triplet[2]-4))
    cbar = fig.colorbar(p, ticks = [global_fit_min, global_fit_max],
                        shrink=0.5)
    cbar.ax.set_yticklabels(['Low', 'High']) # FIXME: doesn't work
    cbar.set_label(r'fitness')
    prepare_legend(plt)
    fname = 'trajectory_sweep_c%d_c%d_c%d.pdf' % tuple((np.array(triplet)-4).tolist())
    plt.savefig(fname, format='pdf')
    plt.close()
    print 'saved %s' % fname


def load_trajectory(fname, triplet):
  """loads the trajectories from the timeline files, returns a
  tuple (x, y, z, fitness) of lists of points (and the fitness column)"""
  lines = open(fname).readlines()
  x = []
  y = []
  z = []
  fit = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    (xi, yi, zi, fit_i) = [float(words[i]) for i in triplet + (1,)]
    x.append(xi)
    y.append(yi)
    z.append(zi)
    fit.append(fit_i)
  return (x, y, z, fit)


def load_timeline(fname):
  """loads the fitness and step size columns from a timeline file;
  returns three lists of values: (iters, fitness, step_size)"""
  lines = open(fname)
  iters = []
  fitness = []
  step_size = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    iters.append(int(words[0]))
    fitness.append(float(words[1]))
    step_size.append(float(words[2]))
  return (iters, fitness, step_size)

def scurve_load_coefficients(outdir, fname):
  """simple helper routine which returns a list of coefficients
  loaded from the given scurve file"""
  c = []
  lines = open(outdir + '/' + fname, 'r').readlines()
  for line in lines:
    if line[0] == '#':
      continue
    c.append(float(line))
  return c

def scurve_apply(s, y_array, L):
  """apply the scurve s to the array y_array; s is a list with the
  coefficients of the polynomial.  L is the scale for y."""
  result = np.zeros_like(y_array)
  for i in range(len(y_array)):
    y = y_array[i]*L/float(len(y_array))
    result[i] = s[0] + s[1]*y + s[2]*y*y + s[3]*y*y*y
  return result

def mask_load(outdir, fname):
  mask = []
  lines = open(outdir + '/' + fname).readlines()
  for line in lines:
    if line[0] == '#':
      continue
    val = int(line)
    assert(val == 0 or val == 1)
    mask.append(val)
  return mask
'''
def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  #plt.legend(loc=loc, shadow=True, ncol=2, prop={'size':8})
  plt.legend(loc='best', fancybox=True, ncol=1, prop={'size':6})
  leg = plt.gca().get_legend()
  ltext = leg.get_texts()
  #plt.setp(ltext, fontsize='smaller')
  leg.get_frame().set_alpha(0.5)
'''
def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  plt.legend(loc=loc, shadow=True, ncol=1, prop={'size':6})
  plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman'],'size':12})
  leg = plt.gca().get_legend()
  ltext = leg.get_texts()
  leg.get_frame().set_alpha(0.2)

def fit_list2fit_delta(fit_list):
  prev_list = np.zeros_like(fit_list)
  prev_list[1:] = fit_list[:-1]
  return fit_list-prev_list


if __name__ == '__main__':
  __MAIN__()
