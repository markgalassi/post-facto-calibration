#! /usr/bin/env python

"""A simple program that plays around with autocorrelations using
python/numpy.  With a normal distribution and 100 points it reproduces
the example in http://en.wikipedia.org/wiki/Autocorrelation"""


import math
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np

x = np.arange(0, 100, 2.0*math.pi/10.0)
si = np.sin(x)                          # sine wave
## random noise, uniform, zero mean
#ra = 1.0*(-0.5 + np.random.random(len(x)))
## random noise, poisson
#ra = np.random.poisson(10.0, len(x))           # random noise
## random noise, gaussian
ra = np.random.normal(0, 1.0, len(x))           # random noise
spn = si + ra                # sine plus noise
print x
print si
print spn

fig = plt.figure(1)
ax1 = fig.add_subplot(3,1,1)
plt.plot(x, si, label='sin(x)')
ax1.legend(loc='lower center', prop={'size': 7})
ax2 = fig.add_subplot(3,1,2)
plt.plot(x, ra, label='random noise')
ax2.legend(loc='lower center', prop={'size': 7})
ax3 = fig.add_subplot(3,1,3)
plt.plot(x, spn, label='sin(x) plus noise')
ax3.legend(loc='lower center', prop={'size': 7})

plt.savefig('play_acorr_f.pdf', format='pdf')
print 'saved play_acorr_f.pdf'

fig = plt.figure(2)
cor_si = np.correlate(si, si, 'full')   # 'full' or 'same'
cor_ra = np.correlate(ra, ra, 'full')
cor_spn = np.correlate(spn, spn, 'full')

ax1 = fig.add_subplot(3,1,1)
plt.bar(np.arange(len(cor_si)), cor_si, label='autocorrelation of sin(x)')
ax1.legend(loc='lower center', prop={'size': 7})
ax2 = fig.add_subplot(3,1,2)
plt.bar(np.arange(len(cor_ra)), cor_ra, label='autocorrelation of noise')
ax2.legend(loc='lower center', prop={'size': 7})
ax3 = fig.add_subplot(3,1,3)
plt.bar(np.arange(len(cor_spn)), cor_spn, label='autocorrelation of sin(x)+noise')
ax3.legend(loc='lower center', prop={'size': 7})

plt.savefig('play_acorr_corr.pdf', format='pdf')
