#!/bin/sh

GNUPLOT="gnuplot"
if [ -f /usr/lanl/bin/gnuplot ] ; then
  GNUPLOT="/usr/lanl/bin/gnuplot"
fi

#make > /dev/null || exit 1
N_STEPS=25000

LIST_GOOD_RUNS=" 0.16,0.83 0.16,2.21 0.41,0.83 0.41,0.12 0.11,0.12 0.25,75,0.83 0.25,75,0.83 0.25,75,2.21 0.39,15,0.83 0.39,15,2.21"
LIST_ALGOS="./scurve-hill ./scurve-adaptive_random ./scurve-iterated ./scurve-variable"
for good_run in $LIST_GOOD_RUNS
do
    for algo in $LIST_ALGOS
    do
	cmd="$algo -g $good_run -n $N_STEPS"
	eval $cmd
    done
done


