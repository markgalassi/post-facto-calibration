#include "scurve-types.h"
#include "prototypes.h"

Source_type source;
double step_size, noise_percentage;
Poly scurve_true, scurve_initial;
int n_steps, real_shift;
int nr_computations;

int main(int argc, char *argv[])
{
  char output_dir[MAX_FILENAME_LEN];
  char fitness_filepath[MAX_FILENAME_LEN], fitness_filepath_only_increases[MAX_FILENAME_LEN];
  double *y_readout = NULL; 
  char run_id_str_eps[MAX_FILENAME_LEN];

  setting_params(argc, argv, hill);  
  make_directories(output_dir); 
  if (is_finite(source)) 
    {
      sprintf(run_id_str_eps, "eps%1.5g_srcY%1.5g_Z%1.5g",  
	      step_size, source.y, source.z);      
    } 
  else {
    sprintf(run_id_str_eps, "eps%1.5g_srcA%1.5g",
	    step_size, source.a);
  }
  sprintf(fitness_filepath, "%s/%s_%s_%s", 
	  output_dir, "hill", run_id_str_eps, "fit_timeline");
  sprintf(fitness_filepath_only_increases, "%s/%s_%s_%s", 
	  output_dir, "hill", run_id_str_eps, "fit_timeline_incr");
  print_metadata(fitness_filepath);

  prepare_mask_and_readout(&y_readout, generate);
  add_noise(noise_percentage, y_readout);

  int step;
  double fitness_next;
  Poly scurve = scurve_initial;
  Poly scurve_next = scurve_initial;
  double entropy = shannon_entropy(hill, scurve_initial, y_readout) ;
  double fitness = calc_fitness(scurve, y_readout);

  double metric;
  for (step = 0; step < n_steps; ++step) {
    /* take a random step */
    scurve_next = hill_take_step(scurve);
    fitness_next = calc_fitness(scurve_next, y_readout);
    /* if fitness has improved, update position in search space */
    if (fitness_next > fitness) {
      /* update fitness */
      fitness = fitness_next;
     /* update scurve */
      metric = metric_coeffs(scurve, scurve_next);

      scurve = scurve_next;
      
      /* updating c0 based on the new location discovered */
      scurve = update_c0(scurve, source, y_readout);
      print_state(fitness_filepath_only_increases, step, fitness, 
		  scurve, step_size, 0, metric);

      if(entropy_indicator){
	entropy = shannon_entropy(hill, scurve, y_readout);
      }
      /* print information if fitness improved*/
      else
	{
	  entropy = 0;
	}
      print_fitness_diagnostics(output_dir, step, scurve, y_readout, hill, 
				step_size); 
    }
    print_state(fitness_filepath, step, fitness, scurve, step_size, entropy, 0);

  }

  printf("hill number computations %d", nr_computations);
  return 0;
}
