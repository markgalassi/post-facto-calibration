/* calculate the shannon entropy for a population of genomes; the
 * equation is -Sum((ni/P) log (ni/P)), where ni is the number of
 * genomes in genotype i, and P is pop_size/2.  I use pop_size/2
 * because my quantities are all "elite averages".
 */
double calc_shannon_entropy(char **population, int n_states, int radius)
{
   char **gt_list;
   int *gt_occupancy, gt_count, gt;
   int i;
   double entropy, occupancy_ratio;

   gt_count = 0;
   gt_list = (char **) malloc(this_run.pop_size/2*sizeof(char *));
   gt_occupancy = (int *) malloc(this_run.pop_size/2*sizeof(int));
   for (i = 0; i < this_run.pop_size/2; ++i) {
   gt_occupancy[i] = 0;
   }
   for (i = 0; i < this_run.pop_size/2; ++i) {
   if ((gt = gt_index(gt_list, gt_count, population[i])) == -1) {
       gt_list[gt_count] = (char *)
                         malloc(this_run.rule_size*sizeof(char));
       copy_rule(population[i], gt_list[gt_count], n_states, radius);
       gt_occupancy[gt_count] = 1;
       ++gt_count;
     } else {
       ++gt_occupancy[gt];
     }
   }

   /* now take the sum over all existing genotypes in the population */
   entropy = 0;
   for (i = 0; i < gt_count; ++i) {
     occupancy_ratio = ((double) gt_occupancy[i])
         /((double) (this_run.pop_size/2));
         entropy = entropy - occupancy_ratio*log(occupancy_ratio);
   }

   for (i = 0; i < gt_count; ++i) {
     free(gt_list[i]);
   }
   free(gt_list);
   free(gt_occupancy);

   return entropy;
}

/* find the index of "rule" in the list of genotypes "gt_list";
 * return -1 if the rule is not in the list.
 */
int gt_index(char **gt_list, int gt_count, char *rule)
{
   int i;
   for (i = 0; i < gt_count; ++i) {
   if (rules_are_identical(rule, gt_list[i], this_run.rule_size)) {
       return i;
       }
   }
   return -1;
}

int rules_are_identical(char *r1, char *r2, int size)
{
   int i;

   for (i = 0; i < size; ++i) {
   if (r1[i] != r2[i]) {
       return 0;
       }
   }
   return 1;
}
