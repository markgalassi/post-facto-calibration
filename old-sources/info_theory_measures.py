#! /usr/bin/env python

"""
Make plots for information theoretic measures
"""

import sys
import glob
import os
import math
from pprint import pprint
import string
import getopt
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import rc
import numpy as np
import pylab
import matplotlib.font_manager as fm
import re

markers = ('^', 'o', 'p', 's', '8', 'h', 'x', 'v')
colormap = cm.jet
correlation_style = 'full'              # 'full' or 'same'
global_fit_max = sys.float_info.min # initial values before we find them
global_fit_min = sys.float_info.max

def __MAIN__():
  ## first check command line options; ADD MORE
  interactive = False
  (opts, args) = getopt.getopt(sys.argv[1:], 'i', [])

  for o, a in opts:
    if o == '-i':                       
      interactive = True

  timeline_fnames = []
  if not args:
    timeline_fnames = glob.glob('outdir_*/*fit_timeline')
    for f in timeline_fnames:
      end = f.index('/')
      outdir_name = f[0:end]
      print 'outdir_name', outdir_name
  else:
    for arg in args:
      if arg[:len('outdir_')] == 'outdir_':
        timeline_fnames.extend(glob.glob(arg + '/*fit_timeline'))
        outdir_name = str(arg)
        print 'outdir_name', outdir_name
      else:
        timeline_fnames.append(arg)
        end = arg.index('/')
        outdir_name = arg[0:end]
        print 'outdir_name', outdir_name

  font = {'family' : 'normal',
          'weight' : 'bold',
          'size'   : 10}

  timeline_fnames.sort()
  pprint('# processing this list of timeline-files:')
  pprint(timeline_fnames)
  ## now the actual work of generating the plots

  epsilon_list = [0, 0.02, 0.03, 0.05, 0.06, 0.1, 0.11, 0.12, 0.16, 0.18, 0.24, 0.25, 0.3, 0.34, 0.38, 0.41, 0.45, 0.48, 0.52, 0.6, 0.7, 0.8, 0.9, 1.0]
  for timeline_ind, tf in enumerate(timeline_fnames):
    plot_measures(outdir_name, timeline_fnames, timeline_ind, tf, epsilon_list)

  plt.close()

  ## now a little feature: if the command line says "interactive" 
  if interactive:
    plt.show()


def plot_measures(outdir_name, timeline_fnames, timeline_ind, tf, epsilon_list):
    # create arrays for each timeline
  H_list = []
  M_list = []
  for epsilon in epsilon_list:
    iters = {}
    fit_list = {}
    fit = {}
    step_size = {} 
    entropy = {}
    (iters[tf], fit[tf], step_size[tf], entropy[tf]) = load_timeline(tf)
    #print iters
    #print entropy
    s = {}
    s_prime = {}
    s = ''
    s_prime = ''
    for j in range(1,(len(fit[tf]))):
      if (fit[tf][j] - fit[tf][j-1]) < -epsilon:
        s+='2' #denoted as \bar{1} in paper
        s_prime+='2'
      elif math.fabs(fit[tf][j] - fit[tf][j-1]) <= epsilon: 
        s+='0'
        s_prime+='0'
      elif (fit[tf][j] - fit[tf][j-1]) > epsilon:
        s+='1'
        s_prime+='1'
               
    #print s
    if len(s) == 0:
      p_01 = p_02 = p_12 = p_10 = p_20 = p_21 = 0
    else:        
      p_01 = float(str(s).count('01'))/float(len(s))
      p_02 = float(str(s).count('02'))/float(len(s))
      p_12 = float(str(s).count('12'))/float(len(s))
      p_10 = float(str(s).count('10'))/float(len(s))
      p_20 = float(str(s).count('20'))/float(len(s))
      p_21 = float(str(s).count('21'))/float(len(s)) 
    #print p_12
    H = 0
    for probs in p_01, p_02, p_12, p_10, p_20, p_21:
      #print probs
      if probs != 0:
        H+=-(probs*math.log(probs,6)) 
        
    #H = H*100    

    s_prime_list = list(s_prime)
      
    for symbol in range(1,len(s_prime_list)):
      if s_prime_list[symbol] == s_prime_list[symbol-1]:
        s_prime_list[symbol] = '7'
        s_prime_list[symbol-1] = '7'
          
    #print s_prime_list
    s_prime = ''.join(s_prime_list)  
    s_prime = re.sub('7','',s_prime)
    s_prime = re.sub('0','',s_prime)
    #print 'lengths', len(s), len(s_prime)
    len_sprime_float = float(len(s_prime))
    len_s_float = float(len(s))
    M=0 
    if len_s_float !=0:
      M = len_sprime_float/len_s_float

    #M = M*100
    M_list.append(M)
    H_list.append(H)
    #print epsilon, H, M

  fig1= plt.figure(1)  
  ax1 = fig1.add_subplot(2,1,1)
  #plt.xscale('log')
  #plt.yscale('log')
  plt.xlabel('epsilon')
  plt.ylabel('info content')
  plt.title('information content as a function of scale factor')
  plt.plot(epsilon_list,H_list,  label = tf[tf.find("/")+1:tf.find("fit_timeline")] , color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
  prepare_legend(plt, loc='best')

  #plt.savefig(tf[:tf.find("/")] + '/' + 'info_measures_H.pdf', format = 'pdf')

  ax1 = fig1.add_subplot(2,1,2)  
  #plt.xscale('log')
  #plt.yscale('log')
  plt.xlabel('epsilon')
  plt.ylabel('partial info content')
  plt.title('partial information content as a function of scale factor')
  plt.plot(epsilon_list,M_list,label = tf[tf.find("/")+1:tf.find("fit_timeline")] , color = plt.get_cmap('jet')(float(timeline_ind)/(len(timeline_fnames))))
  prepare_legend(plt, loc='best')

  plt.savefig(tf[:tf.find("/")] + '/' +  'info_measures.pdf', format = 'pdf')   

  fig2= plt.figure(2)
  #fig3 = plt.figure(3)
  ax2 = fig2.add_subplot(2,1,1)  
  plt.xscale('log')
  plt.xlabel('iteration')
  plt.ylabel('entropy')
  plt.title('entropy vs. iteration')
  plt.plot(iters[tf],entropy[tf], label = tf[tf.find("/")+1:tf.find("fit_timeline")] )
  prepare_legend(plt, loc='best')
  plt.savefig(tf[:tf.find("/")] + '/'  + 'entropy.pdf', format = 'pdf')
  
  #fig4 = plt.figure(4)
  ax2 = fig2.add_subplot(2,1,2)  
  plt.xscale('log')
  plt.xlabel('iteration')
  plt.ylabel('fitness')
  plt.title('fitness vs. iteration')
  plt.plot(iters[tf],fit[tf], label = tf[tf.find("/")+1:tf.find("fit_timeline")] )
  prepare_legend(plt, loc='best')

  plt.savefig(tf[:tf.find("/")] + '/' + 'fit-entr.pdf', format = 'pdf')
  print 'saved plot'

def load_timeline(fname):
  """loads the fitness and step size columns from a timeline file;
  returns three lists of values: (iters, fitness, step_size)"""
  lines = open(fname)
  iters = []
  fitness = []
  step_size = []
  entropy = []
  for line in lines:
    if line[0] == '#':
      continue
    words = string.split(line)
    iters.append(int(words[0]))
    fitness.append(float(words[1]))
    step_size.append(float(words[2]))
    entropy.append(float(words[3])*10)
  return (iters, fitness, step_size, entropy)

def prepare_legend(plt, loc='upper left'):
  """prepare the legend; for our plots we make it somewhat transparent
  and we put it in the upper left corner and we choose a small font
  """
  plt.legend(loc=loc, shadow=True, ncol=2, prop={'size':6})
  plt.rc('font',**{'family':'serif','serif':['Computer Modern Roman'],'size':12})
  leg = plt.gca().get_legend()
  ltext = leg.get_texts()
  leg.get_frame().set_alpha(1.0)

if __name__ == '__main__':
  __MAIN__()
