#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "constants.h"

double function_poly(double x);
double rand_in_bounds(double min, double max);
double calc_entropy(double xi, double epsilon_adapt);

void adapt_prep_step_poly(double xi, double *xi_normal, double *xi_large,
			  int step, double epsilon_adapt, 
			  double *step_size_large);
double adapt_take_step_poly(double xi, int step, double epsilon_adapt);

int main(int argc, char *argv[])
{
  srandom(123456);
  double xi_initial = -1.0;
  double eps_list[] = {0.01, 0.1, 0.5, 1.0, 2.0, 5.0, 10.0};
  int n_epsilons = sizeof(eps_list)/sizeof(eps_list[0]);

  int epsilon_ind, n_steps_ind;
  double epsilon;
  int n_steps = 20000;
  double se = calc_entropy(xi_initial, 0.01);
  /*double xi_large, xi_normal;*/
  double step_size_large;
  double epsilon_adapt;
  printf("# n_step xi fitness se epsilon \n");
  double xi_normal, xi_large;
  /*double step_size_ada*/
  for(epsilon_ind = 0;epsilon_ind<n_epsilons;epsilon_ind++){
    epsilon = eps_list[epsilon_ind];
    epsilon_adapt = epsilon;
    double step_size_adaptive = epsilon;

    printf("# epsilon %g'\n", epsilon); 
    double xi = xi_initial;
    /*double xi_next = xi_initial;*/
    double fitness = function_poly(xi_initial);
    for (n_steps_ind = 0; n_steps_ind < n_steps; ++n_steps_ind) {   
      adapt_prep_step_poly(xi, &xi_normal, &xi_large, n_steps_ind, 
			   epsilon_adapt, &step_size_large); 
      double fitness_pnormal = function_poly(xi_normal);
      double fitness_plarge = function_poly(xi_large);

      static int count_since_last_improvement = 0;
      if (fitness_pnormal > fitness || fitness_plarge > fitness) {
	if (fitness_plarge > fitness_pnormal) 
	  {
	    xi = xi_large;
	    fitness = fitness_plarge; /* (do we need it?) */
	    step_size_adaptive = step_size_large;
	  } 
	else 
	  {
	    xi = xi_normal;
	    fitness = fitness_pnormal; /* (do we need it?) */
	  }

	se = calc_entropy(xi, epsilon_adapt);	
	count_since_last_improvement = 0;
    } 
    else {
      ++count_since_last_improvement;
      if (count_since_last_improvement > nochange_max) 
	{
	  count_since_last_improvement = 0;
	  step_size_adaptive = step_size_adaptive / adapt_step_scaling;
	}
    }
      printf("%d %g %g %g %g\n", n_steps_ind, xi, fitness, se, epsilon);

    }
    printf("\n\n");

      }
 
  return 0;

}

double function_poly(double x){
  return -(x+3.0)*(x-7.0)*(x+9.0)*(x-8.0)*(x-20.0)*(x-2.0);
}

double rand_in_bounds(double min, double max){
  return min + ((max-min)*(float)random())/RAND_MAX;
}

double calc_entropy(double xi, double epsilon_adapt){
  int x_yes, x_no;
  double prob, se;
  x_yes = 0;
  x_no = 0;
  int x_ind;
  double fitness = function_poly(xi);
  int n_entropy_tries = 10000;
  for(x_ind=0; x_ind < n_entropy_tries; x_ind++){
    /* this is the algorithm step */
    double xi_try = adapt_take_step_poly(xi, x_ind, epsilon_adapt);
    if (function_poly(xi_try) > fitness)
      {
	x_yes++;
      }
    else {
      x_no++;
    }
  }         
  prob = (1.0*x_yes)/(1.0*n_entropy_tries);
  if(x_yes == 0)
    se = 0.0;
  else
    se = -prob*log(prob);
  
  return se;  
}


double adapt_take_step_poly(double xi, int step, double epsilon_adapt){
  double xi_current;
  double xi_normal, xi_large;
  double step_size_large;
  adapt_prep_step_poly(xi, &xi_normal, &xi_large, step, epsilon_adapt, &step_size_large);
  double fitness_normal  = function_poly(xi_normal);
  double fitness_large = function_poly(xi_large);
  if(fitness_large > fitness_normal){
    xi_current = xi_large;
    epsilon_adapt = step_size_large;
  }
  else{
    xi_current = xi_normal;
  }

  return xi_current;
  
}

void adapt_prep_step_poly(double xi, double *xi_normal, double *xi_large,
			  int step, double epsilon_adapt, 
			  double *step_size_large){
  *xi_normal+= rand_in_bounds(-epsilon_adapt, epsilon_adapt);
  if (step % large_step_period == 0) 
    {
      *step_size_large = epsilon_adapt * adapt_step_large_scaling;
    } 
  else 
    {
      *step_size_large = epsilon_adapt * adapt_step_small_scaling;
    }
  *xi_large+= rand_in_bounds(-epsilon_adapt, epsilon_adapt);
}


/*
double adapt_take_step(double xi, int step, double epsilon, double *xi_normal, 
		       double *xi_large){
  double xi_current;
  adapt_prep_step_poly(xi, &xi_normal, &xi_large, step, epsilon);
  double fitness_pnormal = function_poly(xi_normal);
  double fitness_plarge = function_poly(xi_large);
  if(fitness_plarge > fitness_pnormal){
    xi_current = xi_large;
  }
  else{
    xi_current = xi_normal;
  }
  return xi_current;

}

void adapt_prep_step_poly(double xi, double *xi_normal, double *xi_large,
			  int step, double epsilon)
{ 
  *xi_normal+= rand_in_bounds(-epsilon, epsilon);
  double step_size_large;
  if (step % large_step_period == 0) 
    {
      step_size_large = epsilon * adapt_step_large_scaling;
      *xi_large = rand_in_bounds(-step_size_large, step_size_large);
    } 
  else 
    {
      step_size_large = epsilon * adapt_step_small_scaling;
      *xi_large+= rand_in_bounds(-step_size_large, step_size_large); 
    }
}
*/
